package voicemybot

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"html"
	"html/template"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strconv"
	"strings"
	"time"

	xhtml "golang.org/x/net/html"

	"appengine"
	"appengine/datastore"
	"appengine/urlfetch"
	"appengine/user"
)

var host = "https://voicemybot.com"

//var host = "https://58ea1bde.ngrok.io"

const maxMessages = 10
const maximumTime = 9223372036854775806

var hipchatUserConfigure = template.Must(template.New("hipchatUserConfigure").Parse(`
<html>
  <head>
 <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.9.0/css/aui.css" media="all">
    <script src="https://www.hipchat.com/atlassian-connect/all.js"></script>
    <link rel="stylesheet" href="https://www.hipchat.com/atlassian-connect/all.css">
	<script>
	var signed_request={{.SignedRequest}}
	function buttonClicked(event, closeDialog) {
		  if (event.action === "dialog.save") {
		    var xmlHttp = new XMLHttpRequest();
    		xmlHttp.open( "GET", '` + host + `/hipchat_save_user_configuration?signed_request='+signed_request, false ); // false for synchronous request
			xmlHttp.send( null );
		  }
		  closeDialog(true); // you can also pass false if you want to prevent the dialog from closing (missing required fields, for instance)
		}
		HipChat.register({
		  "dialog-button-click": buttonClicked
		});
	function voicemybotClearHistory(){
		 var xmlHttp = new XMLHttpRequest();
    		xmlHttp.open( "GET", '` + host + `/hipchat_save_user_configuration?clearHistory=true&signed_request='+signed_request, false ); // false for synchronous request
			xmlHttp.send( null );
			alert("History was cleared");
			{{if .ShowSignButton}}
			close();
			{{end}}
			HipChat.dialog.close();
		}
	function mainAction() {
		  
		    var xmlHttp = new XMLHttpRequest();
    		xmlHttp.open( "GET", '` + host + `/hipchat_save_user_configuration?signed_request='+signed_request, false ); // false for synchronous request
			xmlHttp.send( null );
		  close();
		  
		}
		
HipChat.dialog.updatePrimaryAction({
 
  enabled: {{.ShowSave}}      // or true
});
	</script>
  </head>
  <body>
{{if .ShowClearHistory}} 
<div style="padding: 10px 0; text-align: center;">
<button onclick="voicemybotClearHistory()" class="aui-button">Clear VoiceMyBot message history</button> <br/>
	{{.Data}}
{{if .ShowSignButton}}
{{if .ShowSave}}
<button onclick="mainAction()" class="aui-button">Connect amazon echo</button>
{{end}}
 {{end}}
	</div>
	

{{else}}
<div style="padding: 70px 0; text-align: center;">
	{{.Data}}
{{if .ShowSignButton}}
{{if .ShowSave}}
<button onclick="mainAction()" class="aui-button">Connect amazon echo</button>
{{end}}

 {{end}}
	</div>
	{{end}}
 

</body>
</html>
	
`))

var hipchatAdminPage = template.Must(template.New("hipchatAdminPage").Parse(`
<html>
  <head>
 <link rel="stylesheet" href="//aui-cdn.atlassian.com/aui-adg/5.9.0/css/aui.css" media="all">
    <script src="https://www.hipchat.com/atlassian-connect/all.js"></script>
    <link rel="stylesheet" href="https://www.hipchat.com/atlassian-connect/all.css">
  </head>
  <body>

<h2>Account settings</h2>
    <p>Please use your Google Account in order to link your Alexa and HipChat room.<br/>
		For now, we support linking only via Google Accounts. Sorry for the inconvenience. </p>
	
 	<form action="{{.Host}}/hipchat_doconfigure" method="post" class="aui">
	<div class="field-group">
        <label for="hipChatAdminAcc">Admin account</label>
        <input id="hipChatAdminAcc" class="text long-field" type="text" name="hipChatAdminAcc" value="{{.IntegrationData.AdminName}}"/>
    </div>
	<div class="field-group">
		<label for="UserIDs">User that have access to this add-on. Room users will be able to add themselfs to the list.</label>
        <textarea id="UserIDs" class="textarea" name="UserIDs" placeholder="Enter google accounts of users that will have access to plugin from alexa"/>{{.UserIDs}}</textarea>
	</div>


	<input id="signed_request" type="hidden" name="signed_request" value="{{.Signed_request}}"/>
	<h2>User Feed Updates</h2>
	<p>Please select the users whose updates and posts you don’t want to miss (e.g. PM, Team Lead, etc.).<br/>
		When prompted, VoiceMyBot will update you on the most recent ‘important messages’ (up to 10).</p>
		<br/>
	<fieldset class="group">
	{{$integration :=.IntegrationData }}
	 {{range .RoomMembers.Items}}
	    <div class="checkbox">
		  <label for="subscribe{{.MentionName}}">{{.Name}}</label>
		  <input class="checkbox" type="checkbox" name="subscribe{{.MentionName}}" id= "subscribe{{.MentionName}}" {{if $integration.GotSubscription .MentionName}} checked {{end}}>
		</div>	 
	 {{end}}
	
	</fieldset>
	
	<div class="aui-message Hint" style="width:40%">
	 	<p class="title">
		        <span class="aui-icon icon-hint"></span>
	        	<strong>Sample Interaction</strong>
	    	</p>
		<p>You: <i>Alexa, ask HipChat for important messages</i></p>
		<p>Alexa: <i>Message from PM "We did it, guys! You're the best"</i></p>
	</div>
	
	
	<h2>Notification Feed Updates</h2>
	 <p>Here you can select which integrations to monitor out of all those added to your chat room, and request the latest updates (VoiceMyBot keeps up to 10 most recent notifications). </p>
	<br/>
	<fieldset class="group">
	 {{range .IntegrationData.NotificationDetected}}
		 <div class="checkbox">
			<label for="notification{{.Id}}">{{.Name}}</label>
	        <input class="checkbox" type="checkbox" name="notification{{.Id}}"  id="notification{{.Id}}" {{if .Checked}} checked {{end}}>
	    </div>
	{{end}}
	</fieldset>

	<div class="aui-message Hint" style="width:40%">
	 	<p class="title">
		        <span class="aui-icon icon-hint"></span>
	        	<strong>Sample Interaction</strong>
	    	</p>
		<p>You: <i>Alexa, ask HipChat for a notification feed</i></p>
		<p>Alexa: <i>Notification from: Bamboo, Build #125 failed. 5 out of 5797 failed. Changes by Bruce Lee</i></p>
	</div>
	<div class="aui-message Hint" style="width:40%">
	 	<p class="title">
		        <span class="aui-icon icon-hint"></span>
	        	<strong>Sample Interaction</strong>
	    	</p>
		<p>You: <i>Alexa, ask HipChat for important notifications</i></p>
		<p>Alexa: <i> Notification from: Jira, Feedback Bot created FEEDBACK-62575: Sidebar - Not working</i></p>
	</div>

	
	<h2>Jenkins Settings</h2>
	<p>These commands help users to get status updates of Jenkins jobs, as well as build and deploy source code by voice commands. </p>
	<div class="field-group">
		<label for="JenkinsURL">Jenkins URL</label>
        <input id="JenkinsURL" class="text long-field" type="text" name="JenkinsURL" value="{{.IntegrationData.JenkinsURL}}"/>
	</div>
	<div class="field-group">
		<label for="JenkinsLogin">Jenkins Login</label>
        <input id="JenkinsLogin" class="text long-field" type="text" name="JenkinsLogin" value="{{.IntegrationData.JenkinsLogin}}"/>
	</div>
	<div class="field-group">
		<label for="JenkinsPassword">Jenkins API Token</label>
        <input id="JenkinsPassword" class="text long-field" type="password" name="JenkinsPassword" value=""/>
	</div>
	<div class="field-group">
		<label for="JenkinsBuildJob">Jenkins Build Job Name</label>
        <input id="JenkinsBuildJob" class="text long-field" type="text" name="JenkinsBuildJob" value="{{.IntegrationData.JenkinsBuildJob}}"/>
		{{if ne .JenkinsBuildJobError ""}} 
			<div class="error">{{.JenkinsBuildJobError}}</div>
		{{end}}
	</div>
	<div class="field-group">
		<label for="JenkinsDeployJob">Jenkins Deploy Job Name</label>
        <input id="JenkinsDeployJob" class="text long-field" type="text" name="JenkinsDeployJob" value="{{.IntegrationData.JenkinsDeployJob}}"/>
		{{if ne .JenkinsDeployJobError ""}} 
			<div class="error">{{.JenkinsDeployJobError}}</div>
		{{end}}
	</div>
	<div class="aui-message Hint" style="width:40%">
	 	<p class="title">
		        <span class="aui-icon icon-hint"></span>
	        	<strong>Sample interaction</strong>
	    	</p>
		<p>You: <i>Alexa, ask HipChat to deploy Jenkins</i></p>
		<p>Alexa: <i>Deploy initiated, estimated time 5 minutes 45 seconds</i></p>
	</div>
	<div class="aui-message Hint" style="width:40%">
	 	<p class="title">
		        <span class="aui-icon icon-hint"></span>
	        	<strong>Sample interaction</strong>
	    	</p>
		<p>You: <i>Alexa, ask HipChat when was the last red build</i></p>
		<p>Alexa: <i>The last red build was 3 days ago with 34 of 134 tests failed</i></p>
	</div>
	<div class="aui-message Hint" style="width:40%">
	 	<p class="title">
		        <span class="aui-icon icon-hint"></span>
	        	<strong>Sample interaction</strong>
	    	</p>
		<p>You: <i>Alexa, ask HipChat when was the last build</i></p>
		<p>Alexa: <i>The last build was 3 hours 34 minutes ago with 5 out of 134 test failed. Status Failed</i></p>
	</div>
	
	<h2>Site Pinging</h2>
	<p>You can set up a ping monitoring of your website or web service using our embedded ping service.</p>
	<div class="field-group">
		<label for="SiteURL">URL for the site to ping</label>
        <input id="SiteURL" class="text long-field" type="text" name="SiteURL" value="{{.IntegrationData.SiteURL}}"/>
	</div>
	<div class="field-group">
		<label for="SitePhrase">Phrase in response, meaning that ping is OK</label>
        <input id="SitePhrase" class="text long-field" type="text" name="SitePhrase" value="{{.IntegrationData.SitePhrase}}"/>
	</div>
	<div class="aui-message Hint" style="width:40%">
	 	<p class="title">
		        <span class="aui-icon icon-hint"></span>
	        	<strong>Sample interaction</strong>
	    	</p>
		<p>You: <i>Alexa, ask HipChat about the site status</i></p>
		<p>Alexa: <i>Site is down for 2 hours 45 minutes 13 seconds</i></p>
	</div>
	<h2>Send Fast Reply to a Message</h2>
	<p>If you want to quickly respond to a message, but your hands are full, just tell Alexa to send a short fast response for you.</p>
	<div class="aui-message Hint" style="width:40%">
	 	<p class="title">
		        <span class="aui-icon icon-hint"></span>
	        	<strong>Sample interaction</strong>
	    	</p>
		<p>You: <i>Alexa, ask HipChat to notify that I’ll be 15 minutes late</i></p>
		<p>Alexa: <i>[posts your message in the chat room where integration is enabled]</i></p>
		<p>Alexa: <i>Done</i></p>
	</div>
	<h2>Briefing</h2>
	<p>Provide you a brief summary of all new messages and site status updates.</p>
	<div class="aui-message Hint" style="width:40%">
	 	<p class="title">
		        <span class="aui-icon icon-hint"></span>
	        	<strong>Sample interaction</strong>
	    	</p>
		<p>You: <i>Alexa, ask HipChat what happened</i></p>
		<p>Alexa: <i>You have 0 important messages, 2 notifications, 3 other messages. Site is up and running</i></p>
	</div>
	<h2>Read Last Messages</h2>
	<p>Read the last 10 messages from chat room</p>
	<div class="aui-message Hint" style="width:40%">
	 	<p class="title">
		        <span class="aui-icon icon-hint"></span>
	        	<strong>Sample interaction</strong>
	    	</p>
		<p>You: <i>Alexa, ask HipChat to read new message</i></p>
		<p>Alexa: <i>Message from PM "We did it, guys! You’re the best!" </i></p>
	</div>
	<h2>Mark All as Read</h2>
	<p>Remove all messages and notifications from add-on</p>
	<div class="aui-message Hint" style="width:40%">
	 	<p class="title">
		        <span class="aui-icon icon-hint"></span>
	        	<strong>Sample interaction</strong>
	    	</p>
		<p>You: <i>Alexa, ask HipChat to clear history</i></p>
		<p>Alexa:<i>All messages removed from VoiceMyBot</i></p>
	</div>
	<p>For more information, or to contact us, please visit our website <a href="http://voicemybot.com" target="_blank">VoiceMyBot.com</a></p>
	<div class="buttons-container">
        <div class="buttons">
            <input class="button submit" type="submit" value="Save"/>
        </div>
    </div>
	</form>
  </body>
</html>
`))

// ServerCapabilities is used to get server capabilities request
type ServerCapabilities struct {
	Capabilities struct {
		Oauth2Provider struct {
			AuthorizationUrl string
			TokenUrl         string
		}
		HipchatApiProvider struct {
			URL string
		}
	}
}

// OAuthAccessToken structure is used for handling jwt authentication
type OAuthAccessToken struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   uint32 `json:"expires_in"`
	GroupID     uint32 `json:"group_id"`
	GroupName   string `json:"group_name"`
	Scope       string `json:"scope"`
	TokenType   string `json:"token_type"`
}

// JwtToken structure is used for handling jwt authentication
type JwtToken struct {
	Exp int64
	Iss string
	Jti string
	Sub string
}

// HipchatAdminData is used in hipchatAdminPage template
type HipchatAdminData struct {
	IntegrationData       Integration
	Host                  string
	Signed_request        string
	UserIDs               string
	JenkinsBuildJobError  string
	JenkinsDeployJobError string
	RoomMembers           RoomMembersResponse
}

// HipchatUserConfigureData is used in user-configure dialog template
type HipchatUserConfigureData struct {
	IntegrationData  Integration
	Data             template.HTML
	ShowSave         bool
	ShowClearHistory bool
	ShowSignButton   bool
	SignedRequest    string
}

// NotificationSent is used to send room notifications
type NotificationSent struct {
	Message string `json:"message"`
}

type GlanceElem struct {
	Content struct {
		Label struct {
			Type  string `json:"type"`
			Value string `json:"value"`
		} `json:"label"`
	} `json:"content"`
	Key string `json:"key"`
}
type GlanceSent struct {
	Glance []GlanceElem `json:"glance"`
}

//NotificationReceived Structure used for capturing room notification
//Commented fields are differences between webhook and fetching history
type NotificationReceived struct {
	/*Event string
	Item  struct {*/
	//Message struct {
	Color    string
	Date     string
	From     string
	Id       string
	Mentions []struct {
		Id    int
		Links struct {
			Self string
		}
		MentionName string `json:"mention_name"`
		Name        string
		Version     string
	}
	Message       string
	MessageFormat string `json:"message_format"`
	Type          string
	/*}
	Room struct {
		Id         int
		IsArchived bool `json:"is_archived"`
		Links      struct {
			Members      string
			Participants string
			Self         string
			Webhooks     string
		}
		Name    string
		Privacy string
		Version string
	}
	/*}
	ClientID  string `json:"oauth_client_id"`
	WebhookID int    `json:"webhook_id"`*/
}

//MessageReceived Structure used for capturing room messages
//Commented fields are differences between webhook and fetching history
type MessageReceived struct {
	/*Event string
	Item  struct {*/
	//	Message struct {
	Date string
	File struct {
		Name     string
		Size     int
		ThumbUrl string `json:"thumb_url"`
		Url      string
	}
	From struct {
		//Id    int
		Links struct {
			Self string
		}
		MentionName string `json:"mention_name"`
		Name        string
		Version     string
	}
	Message string
	Type    string
	/*}
	Room struct {
		Id         int
		IsArchived bool `json:"is_archived"`
		Links      struct {
			Members      string
			Participants string
			Self         string
			Webhooks     string
		}
		Name    string
		Privacy string
		Version string
	}
	/*}
	ClientID  string `json:"oauth_client_id"`
	WebhookID int    `json:"webhook_id"`*/
}

//RoomMembersResponse Structure for getting members in the room
type RoomMembersResponse struct {
	Items []struct {
		MentionName string `json:"mention_name"`
		Version     string
		Id          int
		Links       struct {
			Self string
		}
		Name string
	}
	StartIndex int
	MaxResults int
	Links      struct {
		Self string
		Prev string
		Next string
	}
}

//RoomInfoResponse Structure for getting room info
type RoomInfoResponse struct {
	XmppJid string `json:"xmpp_jid"`
	Name    string
}

//RoomHistory Structure for getting room history
type RoomHistory struct {
	Items []map[string]interface{}
}

//Verify jwt signing
func verifyToken(token string, secret string) (bool, string) {
	t := strings.Split(token, ".")
	mac := hmac.New(sha256.New, []byte(secret))
	mac.Write([]byte(t[0] + "." + t[1]))
	expectedMAC := mac.Sum(nil)
	s := base64.StdEncoding.EncodeToString(expectedMAC)
	return strings.HasPrefix(s, t[2]), s
}

// Cleaning user list to remove empty values and duplicates
func cleanUsers(userList []string) []string {
	var result []string
	m := make(map[string]bool)
	for _, item := range userList {
		if strings.TrimSpace(item) == "" {
			continue
		}
		if strings.Contains(item, "@") {
			item = strings.ToLower(strings.TrimSpace(item))
		}
		_, ok := m[strings.TrimSpace(item)]
		if !ok && strings.TrimSpace(item) != "" {
			m[strings.TrimSpace(item)] = true

			result = append(result, strings.TrimSpace(item))
		}
	}
	return result
}

// Send notification to the room identified by the t Integration
func (t *Integration) sendNotification(c appengine.Context, notification string) error {
	t.requestToken(c)
	client := urlfetch.Client(c)
	var requestBody NotificationSent
	requestBody.Message = notification
	binaryResp, err := json.Marshal(requestBody)
	resp, err := client.Post(t.ApiProviderURL+"room/"+strconv.Itoa(t.RoomId)+"/notification?auth_token="+t.AccessToken, "application/json", bytes.NewReader(binaryResp))
	if err != nil {
		c.Errorf("error during performing request %s", err.Error())
		return err
	}
	if resp.StatusCode != 200 && resp.StatusCode != 204 {
		content, readerr := ioutil.ReadAll(resp.Body)
		if readerr != nil {
			content = []byte("Unknown error")
		}
		c.Errorf("Got bad error code sending notifications %d with body %s", resp.StatusCode, content)
		return errors.New("Got bad error code sending notifications")
	}
	return nil
}

// Update glance for the user
func (t *Integration) updateGlance(c appengine.Context, userId string, value string) error {
	t.requestToken(c)
	client := urlfetch.Client(c)
	var requestBody GlanceSent
	var glance GlanceElem
	glance.Key = "voicemybot.glance"
	glance.Content.Label.Type = "html"
	glance.Content.Label.Value = value
	requestBody.Glance = append(requestBody.Glance, glance)
	binaryResp, err := json.Marshal(requestBody)
	resp, err := client.Post(t.ApiProviderURL+"/addon/ui/room/"+strconv.Itoa(t.RoomId)+"/user/"+userId+"?auth_token="+t.AccessToken, "application/json", bytes.NewReader(binaryResp))
	if err != nil {
		c.Errorf("error during performing request %s", err.Error())
		return err
	}
	if resp.StatusCode != 200 && resp.StatusCode != 204 {
		content, readerr := ioutil.ReadAll(resp.Body)
		if readerr != nil {
			content = []byte("Unknown error")
		}
		c.Errorf("Got bad error code updating glance %d with body %s", resp.StatusCode, content)
		return errors.New("Got bad error code updating glance notifications")
	}
	return nil
}

// Get rooms members from the room.
func (t *Integration) getRoomMembers(c appengine.Context) (RoomMembersResponse, error) {
	t.requestToken(c)
	client := urlfetch.Client(c)
	var roomMembersResponse RoomMembersResponse
	resp, err := client.Get(t.ApiProviderURL + "room/" + strconv.Itoa(t.RoomId) + "/member?auth_token=" + t.AccessToken)
	c.Infof(t.ApiProviderURL + "room/" + strconv.Itoa(t.RoomId) + "/participant?auth_token=" + t.AccessToken)
	if err != nil {
		c.Errorf("error during performing request %s", err.Error())
		return roomMembersResponse, err
	}
	if resp.StatusCode != 200 {
		content, readerr := ioutil.ReadAll(resp.Body)

		if readerr != nil {
			content = []byte("Unknown error")
		}
		c.Errorf("Got bad error code receiving room members %d with body %s", resp.StatusCode, content)
		return roomMembersResponse, errors.New("Got bad error code receiving room members")
	}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&roomMembersResponse)
	if err != nil {
		c.Errorf("error during parse response %s", err.Error())
		return roomMembersResponse, err
	}
	return roomMembersResponse, nil
}

// Get rooms participants from the room.
func (t *Integration) getRoomInformation(c appengine.Context) (RoomInfoResponse, error) {
	t.requestToken(c)
	client := urlfetch.Client(c)
	var roomMembersResponse RoomInfoResponse
	resp, err := client.Get(t.ApiProviderURL + "room/" + strconv.Itoa(t.RoomId) + "?auth_token=" + t.AccessToken)
	c.Infof(t.ApiProviderURL + "room/" + strconv.Itoa(t.RoomId) + "?auth_token=" + t.AccessToken)
	if err != nil {
		c.Errorf("error during performing request %s", err.Error())
		return roomMembersResponse, err
	}
	if resp.StatusCode != 200 {
		content, readerr := ioutil.ReadAll(resp.Body)

		if readerr != nil {
			content = []byte("Unknown error")
		}
		c.Errorf("Got bad error code receiving room members %d with body %s", resp.StatusCode, content)
		return roomMembersResponse, errors.New("Got bad error code receiving room members")
	}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&roomMembersResponse)
	if err != nil {
		c.Errorf("error during parse response %s", err.Error())
		return roomMembersResponse, err
	}
	return roomMembersResponse, nil
}

// Get rooms participants from the room.
func (t *Integration) getRoomParticipants(c appengine.Context) (RoomMembersResponse, error) {
	t.requestToken(c)
	client := urlfetch.Client(c)
	var roomMembersResponse RoomMembersResponse
	resp, err := client.Get(t.ApiProviderURL + "room/" + strconv.Itoa(t.RoomId) + "/participant?auth_token=" + t.AccessToken)
	c.Infof(t.ApiProviderURL + "room/" + strconv.Itoa(t.RoomId) + "/participant?auth_token=" + t.AccessToken)
	if err != nil {
		c.Errorf("error during performing request %s", err.Error())
		return roomMembersResponse, err
	}
	if resp.StatusCode != 200 {
		content, readerr := ioutil.ReadAll(resp.Body)

		if readerr != nil {
			content = []byte("Unknown error")
		}
		c.Errorf("Got bad error code receiving room members %d with body %s", resp.StatusCode, content)
		return roomMembersResponse, errors.New("Got bad error code receiving room members")
	}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&roomMembersResponse)
	if err != nil {
		c.Errorf("error during parse response %s", err.Error())
		return roomMembersResponse, err
	}
	return roomMembersResponse, nil
}

//This method checks if request token, stored in integration is still up to date.
//And if not - it will request new token and stores it into Integration
func (t *Integration) requestToken(c appengine.Context) {
	client := urlfetch.Client(c)
	tm := time.Unix(t.ExpirationToken, 0)
	c.Infof("Token expires at %s", tm)
	if time.Since(tm) < time.Duration(30)*time.Second {
		c.Infof("Token is still valid")
		return
	}
	params := url.Values{"grant_type": {"client_credentials"}, "scope": {"send_notification view_messages view_room"}}
	req, err1 := http.NewRequest("POST", t.TokenURL, strings.NewReader(params.Encode()))
	if err1 != nil {
		c.Errorf("error during xreating request %s", err1.Error())
		return
	}
	c.Infof("request id: %s, secret: %s", t.OauthId, t.OauthSecret)
	req.SetBasicAuth(t.OauthId, t.OauthSecret)
	req.Header.Add("Content-Type", `application/x-www-form-urlencoded`)
	resp, err := client.Do(req)
	if err != nil {
		c.Errorf("error during performing request %s", err.Error())
		return
	}
	if resp.StatusCode != 200 {
		content, readerr := ioutil.ReadAll(resp.Body)

		if readerr != nil {
			content = []byte("Unknown error")
		}
		c.Errorf("Got bad error code receiving oauth token %d with body %s", resp.StatusCode, content)

		return
	}
	var token OAuthAccessToken
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&token)
	if err != nil {
		c.Errorf("error during parse response %s", err.Error())
		return
	}
	expirationTime := time.Now().Add(time.Duration(token.ExpiresIn) * time.Second)
	t.ExpirationToken = expirationTime.Unix()
	t.AccessToken = token.AccessToken
	t.save(c)
}

//That method should compute and stores values from hipchat configuration page
func hipchatDoConfigure(w http.ResponseWriter, r *http.Request) {
	var data HipchatAdminData
	c := appengine.NewContext(r)
	r.ParseForm()
	s := r.Form.Get("signed_request")
	data.Signed_request = s
	decodedToken, err := verifySecurity(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	x, _, err := getIntegration(c, decodedToken.Iss)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if x.AdminName != r.Form.Get("hipChatAdminAcc") {
		c.Infof("adminaccount was changed from %s to %s", x.AdminName, r.Form.Get("hipChatAdminAcc"))
		user, key, err := getUserByHipchat(c, x.AdminName)
		if err == nil {
			user.OauthId = ""
			datastore.Put(c, key, &user)
		}
	}
	x.AdminName = r.Form.Get("hipChatAdminAcc")
	if strings.Contains(x.AdminName, "@") {
		x.AdminName = strings.ToLower(r.Form.Get("hipChatAdminAcc"))
	}
	x.JenkinsURL = r.Form.Get("JenkinsURL")
	x.JenkinsLogin = r.Form.Get("JenkinsLogin")
	x.SiteURL = r.Form.Get("SiteURL")
	if x.SiteURL == "" {
		x.SiteLastPingTime = maximumTime
	} else {
		x.SiteLastPingTime = 0
	}
	if r.Form.Get("JenkinsPassword") != "" {
		x.JenkinsPassword = r.Form.Get("JenkinsPassword")
	}
	x.JenkinsBuildJob = r.Form.Get("JenkinsBuildJob")
	x.JenkinsDeployJob = r.Form.Get("JenkinsDeployJob")
	//If user was removed - we should clean his OauthID to remove him from chat
	if strings.Join(x.UserIDs, "\n") != r.Form.Get("UserIDs") {
		c.Infof("User was changed from %s to %s", x.UserIDs, r.Form.Get("UserIDs"))
		tmpUsers := cleanUsers(strings.Split(r.Form.Get("UserIDs"), "\n"))
		for _, u := range x.UserIDs {
			removed := true
		UserLoop:
			for _, tu := range tmpUsers {
				if tu == u {
					removed = false
					break UserLoop
				}
			}
			if removed {
				user, key, err := getUserByHipchat(c, u)
				if err == nil {
					user.OauthId = ""
					datastore.Put(c, key, &user)
				}
			}
		}

	}
	x.UserIDs = cleanUsers(strings.Split(r.Form.Get("UserIDs"), "\n"))
	x.SitePhrase = r.Form.Get("SitePhrase")
	roomMembers, err := x.getRoomMembers(c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	roomParticipants, err := x.getRoomParticipants(c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	for _, participant := range roomParticipants.Items {
		add := true
		for _, member := range roomMembers.Items {
			if member.MentionName == participant.MentionName {
				add = false
				break
			}
		}
		if add {
			roomMembers.Items = append(roomMembers.Items, participant)
		}
	}
	x.Subscriptions = []string{}
	for _, item := range roomMembers.Items {

		if r.Form.Get("subscribe"+item.MentionName) != "" {
			x.Subscriptions = append(x.Subscriptions, item.MentionName)
		}
	}
	sort.Sort(sort.StringSlice(x.Subscriptions))
	for i, item := range x.NotificationDetected {
		if r.Form.Get("notification"+strconv.Itoa(item.Id)) != "" {
			x.NotificationDetected[i].Checked = true
		} else {
			x.NotificationDetected[i].Checked = false
		}
	}
	if x.JenkinsBuildJob != "" {
		_, err = x.getJobInfo(c, x.JenkinsBuildJob)
		x.JenkinsBuildJobOK = true
		if err != nil {
			data.JenkinsBuildJobError = "Error accessing jenkins build job"
			x.JenkinsBuildJobOK = false
		}
	}
	if x.JenkinsDeployJob != "" {
		_, err = x.getJobInfo(c, x.JenkinsDeployJob)
		x.JenkinsDeployJobOK = true
		if err != nil {
			data.JenkinsDeployJobError = "Error accessing jenkins deploy job"
			x.JenkinsDeployJobOK = false
		}
	}
	x.save(c)
	replacedToken := strings.Replace(strings.Replace(s, "_", "/", -1), "-", "+", -1)
	br, generatedToken := verifyToken(replacedToken, x.OauthSecret)
	if !br {
		c.Errorf("token verification fails for: %s. Expected %s, but was %s", x.OauthId, generatedToken, replacedToken)
		http.Error(w, "Token verification fails", http.StatusInternalServerError)
		return
	}

	data.IntegrationData = x
	data.UserIDs = strings.Join(x.UserIDs, "\n")
	data.RoomMembers = roomMembers
	data.Host = host
	if err := hipchatAdminPage.Execute(w, data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//Function is called when the extension is installed in some hipchat room
func hipchatInstalled(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	if r.Method == "DELETE" {
		p := strings.Split(r.URL.Path, "/")[2]
		c.Infof("Deleting of plugin with id %s", p)

		//Removing user association with removed integration.
		qUser := datastore.NewQuery("User").Filter("OauthId =", p)
		var user User
		for t := qUser.Run(c); ; {
			user = User{}
			key, err := t.Next(&user)
			if err == datastore.Done {

				break
			}
			if err != nil {
				c.Errorf(err.Error())
				break
			}
			user.OauthId = ""
			if _, err = datastore.Put(c, key, &user); err != nil {
				c.Errorf(err.Error())

			}
		}

		q := datastore.NewQuery("Integration").Filter("OauthId =", p)
		for t := q.Run(c); ; {
			var x Integration
			key, err := t.Next(&x)
			if err == datastore.Done {
				break
			}
			if err != nil {
				c.Errorf(err.Error())
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			c.Infof("Removing found record from database %s", x.OauthId)
			datastore.Delete(c, key)
		}
		return
	}
	decoder := json.NewDecoder(r.Body)
	var t Integration
	err := decoder.Decode(&t)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	c.Infof("New hipchat installation request with id: %s. Getting capabilities from %s", t.OauthId, t.CapabilitiesUrl)
	client := urlfetch.Client(c)
	resp, err := client.Get(t.CapabilitiesUrl)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if resp.StatusCode != 200 {
		c.Errorf("Got bad error code receiving capabilities %d", resp.StatusCode)
		http.Error(w, "Error getting capabilities", http.StatusInternalServerError)
		return
	}
	var cap ServerCapabilities
	decoder = json.NewDecoder(resp.Body)
	err = decoder.Decode(&cap)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	t.ApiProviderURL = cap.Capabilities.HipchatApiProvider.URL
	t.AuthorizationURL = cap.Capabilities.Oauth2Provider.AuthorizationUrl
	t.TokenURL = cap.Capabilities.Oauth2Provider.TokenUrl
	_, _, err = getIntegration(c, t.OauthId)
	//If integration is not exist
	if err != nil {
		key := datastore.NewIncompleteKey(c, "Integration", voiceMyBotKey(c))
		if _, err := datastore.Put(c, key, &t); err != nil {
			c.Errorf(err.Error())
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

//Function is called when the configuration page is opened in the menu
func hipchatAdmin(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	c.Infof("in admin")
	var data HipchatAdminData
	r.ParseForm()
	s := r.Form.Get("signed_request")
	data.Signed_request = s
	decodedToken, err := verifySecurity(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	x, _, err := getIntegration(c, decodedToken.Iss)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	x.getRoomHistory(c, nil)
	c.Infof("Calling admin page for %s", x.OauthId)
	replacedToken := strings.Replace(strings.Replace(s, "_", "/", -1), "-", "+", -1)
	br, generatedToken := verifyToken(replacedToken, x.OauthSecret)
	if !br {
		c.Errorf("token verification fails for: %s. Expected %s, but was %s", x.OauthId, generatedToken, replacedToken)
		http.Error(w, "Token verification fails", http.StatusInternalServerError)
		return
	}
	data.IntegrationData = x
	data.UserIDs = strings.Join(x.UserIDs, "\n")
	data.Host = host
	roomMembers, err := x.getRoomMembers(c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	roomParticipants, err := x.getRoomParticipants(c)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	for _, participant := range roomParticipants.Items {
		add := true
		for _, member := range roomMembers.Items {
			if member.MentionName == participant.MentionName {
				add = false
				break
			}
		}
		if add {
			roomMembers.Items = append(roomMembers.Items, participant)
		}
	}
	data.RoomMembers = roomMembers
	if err := hipchatAdminPage.Execute(w, data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

//Function is called when there is some room message
// It was modified to be called from fetchRoomHistory function instead of
// webhooks to increase performance
func (integration *Integration) hipchatRoomMessage(c appengine.Context, messageBytes []byte, users *[]User) {

	var message MessageReceived
	err := json.Unmarshal(messageBytes, &message)
	var notification NotificationReceived
	if err != nil {
		err := json.Unmarshal(messageBytes, &notification)
		if err != nil {
			c.Errorf("Error during parsing notification %s", err.Error())
			return
		}
	}
	var msg SavedMessage
	var name string
	if message.Message == "" && message.File.Name == "" {
		c.Infof("New message from %s with text %s", notification.From, notification.Message)
		t, _ := time.Parse(time.RFC3339Nano, notification.Date)
		if notification.MessageFormat == "html" {
			msg = SavedMessage{notification.From,
				t.Unix(),
				html.EscapeString(html2Text(notification.Message)),
				"message"}
		} else {
			msg = SavedMessage{notification.From,
				t.Unix(),
				html.EscapeString(notification.Message),
				"message"}
		}
		name = notification.From
	} else {
		c.Infof("New message from %s with text %s", message.From.Name, message.Message)
		t, err := time.Parse(time.RFC3339Nano, message.Date)
		if err != nil {
			c.Warningf("Error parsing date %s", err.Error())
		}
		if message.Message == "" && message.File.Name != "" {
			msg = SavedMessage{message.From.Name,
				t.Unix(),
				html.EscapeString("Attachment with name " + message.File.Name),
				"message"}

		} else {
			msg = SavedMessage{message.From.Name,
				t.Unix(),
				html.EscapeString(message.Message),
				"message"}
		}
		name = message.From.MentionName
	}
	subscriptedNotification := false
	i := sort.Search(len(integration.Subscriptions), func(i int) bool { return integration.Subscriptions[i] >= name })
	if i < len(integration.Subscriptions) && integration.Subscriptions[i] == name {
		subscriptedNotification = true
	}
	for i := range *users {
		(*users)[i].LastMessages = append((*users)[i].LastMessages, msg)
		if len((*users)[i].LastMessages) > maxMessages {
			(*users)[i].LastMessages = (*users)[i].LastMessages[1:]
		}
		if subscriptedNotification {
			(*users)[i].FollowedMessages = append((*users)[i].FollowedMessages, msg)
			if len((*users)[i].FollowedMessages) > maxMessages {
				(*users)[i].FollowedMessages = (*users)[i].FollowedMessages[1:]
			}
		}
	}
}

// Simple converting html two text
func html2Text(h string) (result string) {
	doc, err := xhtml.Parse(strings.NewReader(h))
	if err != nil {
		return "Failed to parse message"
	}
	var f func(*xhtml.Node, bool)
	result = ""
	f = func(n *xhtml.Node, printText bool) {
		if printText && n.Type == xhtml.TextNode {
			result += fmt.Sprintf("%s ", n.Data)
		}
		printText = printText || (n.Type == xhtml.ElementNode && n.Data == "span")
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c, printText)
		}
	}
	f(doc, true)
	result = strings.TrimSpace(result)
	return
}

//Function is called when there is some room notification
// It was modified to be called from fetchRoomHistory function instead of
// webhooks to increase performance
func (integration *Integration) hipchatRoomNotification(c appengine.Context, notificationBytes []byte, users *[]User) {

	var notification NotificationReceived

	err := json.Unmarshal(notificationBytes, &notification)
	if err != nil {

		c.Errorf("Error during parsing notification %s", err.Error())
		return
	}
	subscriptedNotification := false
	i := sort.Search(integration.NotificationDetected.Len(), func(i int) bool { return integration.NotificationDetected[i].Name >= notification.From })
	if i >= integration.NotificationDetected.Len() || integration.NotificationDetected[i].Name != notification.From {

		integration.NotificationDetected = append(integration.NotificationDetected, NotificationObj{integration.NotificationDetected.Len(), notification.From, false})
		sort.Sort(integration.NotificationDetected)

	} else if i < integration.NotificationDetected.Len() && integration.NotificationDetected[i].Checked {
		subscriptedNotification = true
	}
	t, _ := time.Parse(time.RFC3339Nano, notification.Date)

	msg := SavedMessage{notification.From,
		t.Unix(),
		html.EscapeString(notification.Message),
		"notification"}
	if notification.MessageFormat == "html" {
		msg = SavedMessage{notification.From,
			t.Unix(),
			html.EscapeString(html2Text(notification.Message)),
			"notification"}
	}
	for i := range *users {
		(*users)[i].LastMessages = append((*users)[i].LastMessages, msg)
		if len((*users)[i].LastMessages) > maxMessages {
			(*users)[i].LastMessages = (*users)[i].LastMessages[1:]
		}
		if subscriptedNotification {
			(*users)[i].Notifications = append((*users)[i].Notifications, msg)
			if len((*users)[i].Notifications) > maxMessages {
				(*users)[i].Notifications = (*users)[i].Notifications[1:]
			}
		}
	}
	c.Infof("New notification from %s with text %s", notification.From, notification.Message)
}

//This function will fetch Room history for the previous period. This function break
//history to individual messages and calls required handlers
func (t *Integration) getRoomHistory(c appengine.Context, hu *User) error {
	t.requestToken(c)
	c.Infof("got get history for %s", t.OauthId)
	client := urlfetch.Client(c)
	var roomHistoryResponse RoomHistory
	requestStr := t.ApiProviderURL + "room/" + strconv.Itoa(t.RoomId) + "/history?auth_token=" + t.AccessToken + "&max-results=500"
	if t.LastMessageTakes != 0 {
		requestStr += "&end-date=" + strings.Replace(time.Unix(t.LastMessageTakes, 0).Format(time.RFC3339Nano), "+", "%2B", -1)
	}
	c.Infof("Request str %s", requestStr)
	t.LastMessageTakes = time.Now().Unix()
	resp, err := client.Get(requestStr)
	if err != nil {
		c.Errorf("error during performing request %s", err.Error())
		return err
	}
	if resp.StatusCode != 200 {
		content, readerr := ioutil.ReadAll(resp.Body)
		if readerr != nil {
			content = []byte("Unknown error")
		}
		c.Errorf("Got bad error code receiving room members %d with body %s", resp.StatusCode, content)
		return errors.New("Got bad error code receiving room members")
	}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&roomHistoryResponse)
	if err != nil {
		c.Errorf("error during parse response %s", err.Error())
		return err
	}
	var users []User
	q := datastore.NewQuery("User").Filter("OauthId =", t.OauthId)
	var x User
	for t := q.Run(c); ; {
		x = User{}
		_, err := t.Next(&x)
		if err == datastore.Done {

			break
		}
		if err != nil {
			c.Errorf(err.Error())
			break
		}
		if hu != nil && x.UserId == hu.UserId {
			users = append(users, *hu)
		} else {
			users = append(users, x)
		}
	}
	c.Infof("Got %d messages in the room ", len(roomHistoryResponse.Items))
	for _, item := range roomHistoryResponse.Items {

		msgType := fmt.Sprint((map[string]interface{}(item))["type"])
		bytes, err := json.Marshal(item)
		if err != nil {
			c.Errorf("Error marshaling message %s", err.Error())
		}
		switch {
		case msgType == "message":
			t.hipchatRoomMessage(c, bytes, &users)
		case msgType == "notification":
			t.hipchatRoomNotification(c, bytes, &users)
		}
	}
	for i := range users {
		if hu != nil && x.UserId == hu.UserId {
			hu.LastMessages = users[i].LastMessages
			hu.FollowedMessages = users[i].FollowedMessages
			hu.Notifications = users[i].Notifications
		}
		users[i].save(c)

	}
	t.save(c)
	return nil
}

func verifySecurity(r *http.Request) (JwtToken, error) {
	c := appengine.NewContext(r)
	r.ParseForm()
	s := r.Form.Get("signed_request")
	var decodedToken JwtToken
	jwt := strings.Split(s, ".")
	djwt, err := base64.StdEncoding.DecodeString(jwt[1])
	if err != nil {
		jwt = strings.Split(s, ".")
		djwt, err = base64.StdEncoding.DecodeString(jwt[1] + "=")
		if err != nil {
			jwt = strings.Split(s, ".")
			djwt, err = base64.StdEncoding.DecodeString(jwt[1] + "==")
			if err != nil {
				c.Warningf("Error trying to decode %s", jwt[1])
				return decodedToken, err
			}
		}
	}

	decoder := json.NewDecoder(bytes.NewReader(djwt))
	err = decoder.Decode(&decodedToken)

	tm := time.Unix(decodedToken.Exp, 0)
	c.Infof("%s", tm)
	if time.Since(tm) >= time.Duration(350)*time.Second {
		c.Errorf("Request is too old %s", tm)
		return decodedToken, errors.New("Request is too old")
	}
	return decodedToken, nil
}

// This function check whehter current user is attached to chat and if not - allow user to attach.
func hipchatUserConfiguration(w http.ResponseWriter, r *http.Request) {
	var data HipchatUserConfigureData
	data.ShowSave = false

	c := appengine.NewContext(r)
	u := user.Current(c)
	decodedToken, err := verifySecurity(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if r.Form.Get("clientStandalone") == "true" {
		data.ShowSignButton = true
	}
	var user User
	var userKey *datastore.Key
	if u == nil {
		user, userKey, err = getUserByInternalHipchat(c, decodedToken.Sub)
		if err != nil || user.OauthId != decodedToken.Iss {
			if strings.Contains(r.UserAgent(), "HipChat") {
				data.Data = (template.HTML)(`<h1>Please open <a href="` + host + `/hipchat_user_configuration?clientStandalone=true&signed_request=` + r.Form.Get("signed_request") + `" target="_blank">configuration page</a> </h1>`)
			} else {
				data.Data = (template.HTML)(`<h1>Please <a href="` + host + `/userLoginPage" target="_blank">login</a> to your google account and then refresh the page.</h1>`)
			}
			if err := hipchatUserConfigure.Execute(w, data); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}
	} else {
		user, userKey, err = getUserByHipchat(c, u.Email)
		if err != nil {
			data.Data = `<h1>Please link your google account to amazon echo device. <a href="http://demo.softserveinc.com/voicemybot/step-by-step-instructions/" target="_blank">Instructions. </a> </h1>`
			if err := hipchatUserConfigure.Execute(w, data); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return
		}
	}

	data.SignedRequest = r.Form.Get("signed_request")
	ok := false
	for _, id := range user.HipChatIds {
		if id == decodedToken.Sub {
			ok = true
		}
	}

	x, _, err := getIntegration(c, decodedToken.Iss)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if !ok {
		roomInfo, err := x.getRoomInformation(c)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		user.HipChatIds = append(user.HipChatIds, decodedToken.Sub)
		user.ActiveChats = append(user.ActiveChats, ActiveChat{decodedToken.Sub, roomInfo.Name})
		if _, err = datastore.Put(c, userKey, &user); err != nil {
			c.Errorf(err.Error())
		}
	}

	if user.OauthId != decodedToken.Iss {
		data.Data = `<h1>Your account is not connected to the current chat room. Press "Connect amazon echo" to connect your alexa to current chat room.</h1>`
		data.ShowSave = true
	} else {
		data.Data = `<h1>Your account is connected to the current chat room</h1>`
		data.ShowClearHistory = true
		if u == nil {
			x.updateGlance(c, decodedToken.Sub, "Connected as "+user.Name)
		} else {
			x.updateGlance(c, decodedToken.Sub, "Connected as "+u.String())
		}
	}
	if err := hipchatUserConfigure.Execute(w, data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

}

// for now this function attach user to the specified chat
func hipchatUserConfigurationSave(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := user.Current(c)
	decodedToken, err := verifySecurity(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var user User
	var userKey *datastore.Key
	if u == nil {
		if r.Form.Get("clearHistory") == "true" {
			user, userKey, err = getUserByInternalHipchat(c, decodedToken.Sub)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		} else {
			return
		}
	} else {
		user, userKey, err = getUserByHipchat(c, u.Email)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	x, key, err := getIntegration(c, decodedToken.Iss)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	c.Infof("Updating " + user.OauthId + "|" + x.OauthId)
	user.OauthId = x.OauthId
	if _, err = datastore.Put(c, userKey, &user); err != nil {
		c.Errorf(err.Error())
		return
	}
	if u != nil {
		ok := false
		for _, item := range x.UserIDs {
			if item == strings.ToLower(u.Email) {
				ok = true
				break
			}
		}
		if !ok {
			x.UserIDs = append(x.UserIDs, strings.ToLower(u.Email))
			if _, err = datastore.Put(c, key, &x); err != nil {
				c.Errorf(err.Error())
				return
			}
		}
	}
	if r.Form.Get("clearHistory") == "true" && userKey != nil {
		x.getRoomHistory(c, &user)
		user.LastMessages = []SavedMessage{}
		user.FollowedMessages = []SavedMessage{}
		user.Notifications = []SavedMessage{}
		if _, err = datastore.Put(c, userKey, &user); err != nil {
			c.Errorf(err.Error())
		}
	}
	if u != nil {
		x.updateGlance(c, decodedToken.Sub, "Connected as "+u.String())
	}

}

func userLoginPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `
					<html>
					<script>
					close();
					</script>
					<body>
					<h1>You could close this page.</h1>
					</body>
					</html>
				`)

}

//That function will check status of connection.
func hipchatGlance(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	decodedToken, err := verifySecurity(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	x, _, err := getUserByInternalHipchat(c, decodedToken.Sub)
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if err != nil {
		u := user.Current(c)
		if u != nil {
			user, userKey, err := getUserByHipchat(c, u.Email)
			if err == nil {
				ok := false
				for _, id := range user.HipChatIds {
					if id == decodedToken.Sub {
						ok = true
					}
				}

				x, _, err := getIntegration(c, decodedToken.Iss)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				if !ok {
					roomInfo, err := x.getRoomInformation(c)
					if err != nil {
						http.Error(w, err.Error(), http.StatusInternalServerError)
						return
					}
					user.HipChatIds = append(user.HipChatIds, decodedToken.Sub)
					user.ActiveChats = append(user.ActiveChats, ActiveChat{decodedToken.Sub, roomInfo.Name})
					if _, err = datastore.Put(c, userKey, &user); err != nil {
						c.Errorf(err.Error())
					}
				}
				if user.OauthId == decodedToken.Iss {
					fmt.Fprintf(w, `
					{
					  "label": {
					    "type": "html",
					    "value": "Connected as `+user.Name+`"
					  }
					 
					}
				`)
					return
				} else {

					fmt.Fprintf(w, `
						{
						  "label": {
						    "type": "html",
						    "value": "<b>Used in another chat</b>"
						  }
						 
						}
					`)
					return
				}
			}
		}

		fmt.Fprintf(w, `
			{
			  "label": {
			    "type": "html",
			    "value": "<b>Not connected</b> "
			  }
			 
			}
		`)
		return
	}

	if x.OauthId == decodedToken.Iss {
		fmt.Fprintf(w, `
		{
		  "label": {
		    "type": "html",
		    "value": "Connected as `+x.Name+`"
		  }
		 
		}
	`)
	} else {
		c.Infof(x.OauthId + "|" + decodedToken.Iss)
		fmt.Fprintf(w, `
		{
		  "label": {
		    "type": "html",
		    "value": "<b>Used in another chat</b>"
		  }
		 
		}
	`)
	}

}

// Hipchat descripttor handlers. All it do is just returning descriptor JSON
func hipchatDescriptor(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `
		{
		  "name": "VoiceMyBot",
		  "key": "com.softserveinc.voicemybot",
		  "description": "SoftServe's VoiceMyBot enables access to all Atlassian HipChat Marketplace listings from a single place, by using voice commands. Connect your Alexa to the required HipChat room and configure your settings to receive only relevant information.",
		  "links":{
				"homepage":"`+host+`",
				"self":"`+host+`/hipchat_descriptor"
			},
		  "capabilities": {
				"configurable":{
					"url": "`+host+`/hipchat_admin"
				},
				"installable": {
			      "allowGlobal": false,
			      "allowRoom": true,
			      "callbackUrl": "`+host+`/hipchat_installed"
			    },
		  "glance": [
		        {
		            "name": {
		                "value": "An addon glance"   
		            },
		            "queryUrl": "`+host+`/hipchat_glance",
		            "key": "voicemybot.glance",
		            "target": "voicemybot.user-configuration",     
		            "icon": {
		               "url":"`+host+`/VmyBot_icon_64x64@1x.jpg",
						"url@2x":"`+host+`/VmyBot_icon_64x64@2x.jpg"
		            },
		            "conditions": [
		            ]
		        }
			],
				"dialog":[
				{
					"key": "voicemybot.user-configuration",
					"title": {
						"value":"VoiceMyBot Configuration"
					},
					"options": {
					"primaryAction": {
						"name": {
	          				"value": "Connect amazon echo"
	        			},
	        			"key": "dialog.save",
						"enabled": true 
					}
					},
					"url":"`+host+`/hipchat_user_configuration"
				}
				],
				
			    "hipchatApiConsumer": {
					"avatar":{
					"url":"`+host+`/VmyBot_icon_64x64@1x.jpg",
					"url@2x":"`+host+`/VmyBot_icon_64x64@2x.jpg"
					},
			      "scopes": [
			        "send_notification",
					"view_messages",
					"view_room"
			      ]
				}
			},
		    
		  "vendor": {
			"name":"softserve",
			"url":"www.softserveinc.com"
			}
		}
	`)
}
