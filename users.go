package voicemybot

import (
	"appengine"
	"appengine/datastore"
)

//Search user by google id.
func getUserByHipchat(c appengine.Context, hipchatId string) (User, *datastore.Key, error) {
	q := datastore.NewQuery("User").Filter("UserId =", hipchatId)
	var x User
	for t := q.Run(c); ; {
		x = User{}
		key, err := t.Next(&x)
		if err == datastore.Done {
			c.Errorf("No hipchat user found with id: %s", hipchatId)
			return x, nil, err
		}
		if err != nil {
			c.Errorf(err.Error())
			return x, nil, err
		}
		return x, key, nil
	}
}

//Search user by hipchat id.
func getUserByInternalHipchat(c appengine.Context, hipchatId string) (User, *datastore.Key, error) {
	q := datastore.NewQuery("User").Filter("HipChatIds =", hipchatId)
	var x User
	for t := q.Run(c); ; {
		x = User{}
		key, err := t.Next(&x)
		if err == datastore.Done {
			c.Errorf("No hipchat user found with id: %s", hipchatId)
			return x, nil, err
		}
		if err != nil {
			c.Errorf(err.Error())
			return x, nil, err
		}
		return x, key, nil
	}
}

//Save the user. It performs getUserByHipchat to find the full key
func (user *User) save(c appengine.Context) {
	_, userKey, err := getUserByHipchat(c, user.UserId)
	if _, err = datastore.Put(c, userKey, user); err != nil {
		c.Errorf(err.Error())
	}
}

//Get the integration for the user. If user do not have integration it tries to find one.
func (user *User) getIntegration(c appengine.Context) (Integration, *datastore.Key, error) {
	var tmp Integration
	_, userKey, err := getUserByHipchat(c, user.UserId)
	if err != nil {
		return tmp, userKey, err
	}
	if user.OauthId != "" {
		integration, key, err := getIntegration(c, user.OauthId)
		if key != nil {
			return integration, key, err
		}
		user.OauthId = ""
	}
	integration, key, err := getIntegrationByUserId(c, user.UserId)
	if key != nil {
		user.OauthId = integration.OauthId
	} else {
		integration, key, err := getIntegrationByUserId(c, user.Name)
		if key != nil {
			user.OauthId = integration.OauthId
		} else {
			return integration, nil, err
		}
	}
	if _, err = datastore.Put(c, userKey, user); err != nil {
		c.Errorf(err.Error())
		return tmp, nil, err
	}
	return integration, key, nil
}

//Create new user. This method is called when alexa user is performing account linking
func createUser(c appengine.Context, userId string, amazonId string, userName string) *datastore.Key {
	_, key, err := getUserByHipchat(c, userId)
	if key != nil && err == nil {
		return nil //User already exists
	}
	var t User
	t.UserId = userId
	t.AmazonID = amazonId
	t.Name = userName
	integration, key, _ := getIntegrationByUserId(c, userId)
	if key != nil {
		t.OauthId = integration.OauthId
	} else {
		integration, key, _ := getIntegrationByUserId(c, userName)
		if key != nil {
			t.OauthId = integration.OauthId
		}
	}
	key = datastore.NewIncompleteKey(c, "User", voiceMyBotKey(c))
	key, err = datastore.Put(c, key, &t)
	if err != nil {
		c.Errorf(err.Error())
		return nil
	}
	return key
}
