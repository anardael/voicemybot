package voicemybot

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"appengine"
	"appengine/urlfetch"
)

// JenkinsLastSuccessfulBuild Structure specify jenkins build json
type JenkinsLastSuccessfulBuild struct {
	Actions           []map[string]interface{} `json:"actions"`
	Artifacts         []interface{}            `json:"artifacts"`
	Building          bool                     `json:"building"`
	Description       interface{}              `json:"description"`
	DisplayName       string                   `json:"displayName"`
	Duration          int                      `json:"duration"`
	EstimatedDuration int                      `json:"estimatedDuration"`
	Executor          interface{}              `json:"executor"`
	FullDisplayName   string                   `json:"fullDisplayName"`
	ID                string                   `json:"id"`
	KeepLog           bool                     `json:"keepLog"`
	Number            int                      `json:"number"`
	QueueID           int                      `json:"queueId"`
	Result            string                   `json:"result"`
	Timestamp         int64                    `json:"timestamp"`
	URL               string                   `json:"url"`
	BuiltOn           string                   `json:"builtOn"`
	ChangeSet         struct {
		Items []interface{} `json:"items"`
		Kind  interface{}   `json:"kind"`
	} `json:"changeSet"`
	Culprits []interface{} `json:"culprits"`
}

// JenkinsJobInfo Structure specify jenkins job json
type JenkinsJobInfo struct {
	Actions []struct {
		ParameterDefinitions []struct {
			DefaultParameterValue struct {
				Value string `json:"value"`
			} `json:"defaultParameterValue"`
			Description string `json:"description"`
			Name        string `json:"name"`
			Type        string `json:"type"`
		} `json:"parameterDefinitions"`
	} `json:"actions"`
	Description       string      `json:"description"`
	DisplayName       string      `json:"displayName"`
	DisplayNameOrNull interface{} `json:"displayNameOrNull"`
	Name              string      `json:"name"`
	URL               string      `json:"url"`
	Buildable         bool        `json:"buildable"`
	Builds            []struct {
		Number int    `json:"number"`
		URL    string `json:"url"`
	} `json:"builds"`
	Color      string `json:"color"`
	FirstBuild struct {
		Number int    `json:"number"`
		URL    string `json:"url"`
	} `json:"firstBuild"`
	HealthReport []struct {
		Description   string `json:"description"`
		IconClassName string `json:"iconClassName"`
		IconURL       string `json:"iconUrl"`
		Score         int    `json:"score"`
	} `json:"healthReport"`
	InQueue          bool `json:"inQueue"`
	KeepDependencies bool `json:"keepDependencies"`
	LastBuild        struct {
		Number int    `json:"number"`
		URL    string `json:"url"`
	} `json:"lastBuild"`
	LastCompletedBuild struct {
		Number int    `json:"number"`
		URL    string `json:"url"`
	} `json:"lastCompletedBuild"`
	LastFailedBuild struct {
		Number int    `json:"number"`
		URL    string `json:"url"`
	} `json:"lastFailedBuild"`
	LastStableBuild struct {
		Number int    `json:"number"`
		URL    string `json:"url"`
	} `json:"lastStableBuild"`
	LastSuccessfulBuild struct {
		Number int    `json:"number"`
		URL    string `json:"url"`
	} `json:"lastSuccessfulBuild"`
	LastUnstableBuild     interface{} `json:"lastUnstableBuild"`
	LastUnsuccessfulBuild struct {
		Number int    `json:"number"`
		URL    string `json:"url"`
	} `json:"lastUnsuccessfulBuild"`
	NextBuildNumber int `json:"nextBuildNumber"`
	Property        []struct {
		ParameterDefinitions []struct {
			DefaultParameterValue struct {
				Name  string `json:"name"`
				Value string `json:"value"`
			} `json:"defaultParameterValue"`
			Description string `json:"description"`
			Name        string `json:"name"`
			Type        string `json:"type"`
		} `json:"parameterDefinitions"`
	} `json:"property"`
	QueueItem          interface{}   `json:"queueItem"`
	ConcurrentBuild    bool          `json:"concurrentBuild"`
	DownstreamProjects []interface{} `json:"downstreamProjects"`
	Scm                struct {
	} `json:"scm"`
	UpstreamProjects []interface{} `json:"upstreamProjects"`
}

// createClient is urlfetch.Client with Deadline
func createClient(context appengine.Context, t time.Duration) *http.Client {
	return &http.Client{
		Transport: &urlfetch.Transport{
			Context:  context,
			Deadline: t,
		},
	}
}

// Function will fetch tests result from jobinfo
func (inf JenkinsLastSuccessfulBuild) getTestResult() (result string) {
	totalTests := 0
	failedTests := 0
	result = ""
	for _, action := range inf.Actions {
		if val, ok := action["urlName"]; ok {
			if val.(string) != "testReport" {
				continue
			}
			totalTests = int(action["totalCount"].(float64))
			failedTests = int(action["failCount"].(float64))
		}
	}
	if totalTests != 0 {
		result = fmt.Sprintf("with %d of %d test failed", failedTests, totalTests)
	}
	return
}

func (t *Integration) getLastSuccessfullBuild(c appengine.Context, jobName string) (JenkinsLastSuccessfulBuild, error) {
	return t.getLastBuildModificator(c, jobName, "lastSuccessfulBuild")
}

func (t *Integration) getLastFailedBuild(c appengine.Context, jobName string) (JenkinsLastSuccessfulBuild, error) {
	return t.getLastBuildModificator(c, jobName, "lastFailedlBuild")
}

func (t *Integration) getLastBuild(c appengine.Context, jobName string) (JenkinsLastSuccessfulBuild, error) {
	return t.getLastBuildModificator(c, jobName, "lastBuild")
}

//This function will return information for last build depending on modificator
func (t *Integration) getLastBuildModificator(c appengine.Context, jobName string, modificatorName string) (JenkinsLastSuccessfulBuild, error) {
	client := createClient(c, time.Second*2)
	var job JenkinsLastSuccessfulBuild
	req, err := http.NewRequest("GET", t.JenkinsURL+"/job/"+jobName+"/"+modificatorName+"/api/json", nil)
	if err != nil {
		c.Errorf("error during xreating request %s", err.Error())
		return job, err
	}

	req.SetBasicAuth(t.JenkinsLogin, t.JenkinsPassword)
	req.Header.Add("Content-Type", `application/x-www-form-urlencoded`)
	resp, err := client.Do(req)
	if err != nil {
		c.Errorf("error during performing request %s", err.Error())
		return job, err
	}
	if resp.StatusCode != 200 {
		content, readerr := ioutil.ReadAll(resp.Body)

		if readerr != nil {
			content = []byte("Unknown error")
		}
		c.Errorf("Got bad error code receiving jenkins response %d with body %s", resp.StatusCode, content)

		return job, errors.New("Got bad error code receiving jenkins response")
	}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&job)
	if err != nil {
		c.Errorf("error during parse response %s", err.Error())
		return job, err
	}
	return job, nil
}

//This function will return information for the job
func (t *Integration) getJobInfo(c appengine.Context, jobName string) (JenkinsJobInfo, error) {
	client := createClient(c, time.Second*2)
	var job JenkinsJobInfo
	req, err := http.NewRequest("GET", t.JenkinsURL+"/job/"+jobName+"/api/json", nil)
	if err != nil {
		c.Errorf("error during xreating request %s", err.Error())
		return job, err
	}
	req.SetBasicAuth(t.JenkinsLogin, t.JenkinsPassword)
	req.Header.Add("Content-Type", `application/x-www-form-urlencoded`)
	resp, err := client.Do(req)
	if err != nil {
		c.Errorf("error during performing request %s", err.Error())
		return job, err
	}
	if resp.StatusCode != 200 {
		content, readerr := ioutil.ReadAll(resp.Body)

		if readerr != nil {
			content = []byte("Unknown error")
		}
		c.Errorf("Got bad error code receiving jenkins response %d with body %s", resp.StatusCode, content)
		return job, errors.New("Got bad error code receiving jenkins response")
	}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&job)
	if err != nil {
		c.Errorf("error during parse response %s", err.Error())
		return job, err
	}
	return job, nil
}

//Execute jenkins job. Jenkins configuraton should be in Integration object
func (t *Integration) executeJob(c appengine.Context, jobName string) error {
	client := createClient(c, time.Second*2)

	req, err := http.NewRequest("POST", t.JenkinsURL+"/job/"+jobName+"/build?delay=0sec", nil)
	if err != nil {
		c.Errorf("error during creating request %s", err.Error())
		return err
	}
	req.SetBasicAuth(t.JenkinsLogin, t.JenkinsPassword)
	req.Header.Add("Content-Type", `application/x-www-form-urlencoded`)
	resp, err := client.Do(req)
	if err != nil {
		c.Errorf("error during performing request %s", err.Error())
		return err
	}
	if resp.StatusCode != 200 && resp.StatusCode != 201 {
		content, readerr := ioutil.ReadAll(resp.Body)
		if readerr != nil {
			content = []byte("Unknown error")
		}
		c.Errorf("Got bad error code receiving jenkins response %d with body %s", resp.StatusCode, content)
		return errors.New("Got bad error code receiving jenkins response")
	}
	return nil
}
