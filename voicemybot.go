package voicemybot

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"appengine"
	"appengine/datastore"
)

//NotificationObj structure is used to store notification subsription list
type NotificationObj struct {
	Id      int
	Name    string
	Checked bool
}

//NotificationByName is Array of notification object new structure is done for sorting by name
type NotificationByName []NotificationObj

//Integration is main structure in the application. The structure describe hipchat integration
type Integration struct {
	AdminName        string
	OauthId          string
	CapabilitiesUrl  string
	RoomId           int
	GroupId          int
	OauthSecret      string
	UserIDs          []string
	AuthorizationURL string
	TokenURL         string

	ApiProviderURL string

	//Jenkins settings
	JenkinsURL         string
	JenkinsLogin       string
	JenkinsPassword    string
	JenkinsBuildJob    string
	JenkinsDeployJob   string
	JenkinsBuildJobOK  bool
	JenkinsDeployJobOK bool

	//Site pinging block
	SitePhrase       string
	SiteURL          string
	SiteLastPingTime int64
	SiteStatus       string
	SiteDownTime     int64

	AccessToken     string
	ExpirationToken int64

	LastMessageTakes int64

	Subscriptions        []string
	NotificationDetected NotificationByName
}

//SavedMessage stores recent message, notification or message from follower
type SavedMessage struct {
	From    string
	Date    int64
	Message string
	Type    string
}

//ActiveChat contains information about the room user is part of
type ActiveChat struct {
	Id          string
	Description string
}

//User structure represent alexa user
type User struct {
	UserId           string
	AmazonID         string
	RoomID           string
	OauthId          string
	Name             string
	HipChatIds       []string
	ActiveChats      []ActiveChat
	Notifications    []SavedMessage
	FollowedMessages []SavedMessage
	LastMessages     []SavedMessage
}

//TemplateData structure  used for admin page template
type TemplateData struct {
	LogoutURL string
}

//WelcomeData structure  used for welcomePage template
type WelcomeData struct {
	LoginURL string
}

//template is used to represent the welcome page after user logs in.
// Is not used in this version
var adminPage = template.Must(template.New("admin").Parse(`
<html>
  <head>
    <title>Alexa Hipchat integration</title>
  </head>
  <body>
	<a href="{{.LogoutURL}}">sign out</a>
    Hello world!!!
  </body>
</html>
`))

//template is used to represent the welcome page.
// Is not used in this version since it is redirecting user to demo.softserveinc.com
var welcomePage = template.Must(template.New("welcome").Parse(`
<html>
  <head>
    <title>Alexa Hipchat integration</title>
  </head>
  <body>
	Please login to the application: <a href="{{.LoginURL}}">Sign in or register</a>
  </body>
</html>
`))

func (a NotificationByName) Len() int           { return len(a) }
func (a NotificationByName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a NotificationByName) Less(i, j int) bool { return a[i].Name < a[j].Name }

//Returns teh integration by OauthID. In case if integration is not found - error is returned
func getIntegration(c appengine.Context, id string) (Integration, *datastore.Key, error) {
	q := datastore.NewQuery("Integration").Filter("OauthId =", id)
	var x Integration
	for t := q.Run(c); ; {
		x = Integration{}
		key, err := t.Next(&x)
		if err == datastore.Done {
			c.Errorf("No hipchat installation found with id: %s", id)
			return x, nil, err
		}
		if err != nil {
			c.Errorf(err.Error())
			return x, nil, err
		}
		return x, key, nil
	}
}

//returns the integration by google ID. Only one integration is returned.
func getIntegrationByUserId(c appengine.Context, userId string) (Integration, *datastore.Key, error) {
	q := datastore.NewQuery("Integration").Filter("AdminName =", userId)
	var x Integration
	for t := q.Run(c); ; {
		x = Integration{}
		key, err := t.Next(&x)
		if err == datastore.Done {
			break
		}
		if err != nil {
			c.Errorf(err.Error())
			return x, nil, err
		}
		return x, key, nil
	}
	q = datastore.NewQuery("Integration").Filter("UserIDs =", userId)
	for t := q.Run(c); ; {
		x = Integration{}
		key, err := t.Next(&x)
		if err == datastore.Done {
			c.Errorf("No hipchat installation found with user: %s", userId)
			return x, nil, err
		}
		if err != nil {
			c.Errorf(err.Error())
			return x, nil, err
		}
		return x, key, nil
	}
}

//GotSubscription Check whether the follower is in the subscription list
func (t Integration) GotSubscription(s string) bool {
	for _, subscription := range t.Subscriptions {
		if subscription == s {
			return true
		}
	}
	return false
}

//Saving the integration to datastore. This integration performs fetching as well,
// So it is better to perform datastore.put method where it is possible
func (t *Integration) save(c appengine.Context) error {
	_, key, err := getIntegration(c, t.OauthId)
	if err != nil {
		return err
	}
	if _, err := datastore.Put(c, key, t); err != nil {
		return err
	}
	return nil
}

//Returns incomplete key for voicemybot entities
func voiceMyBotKey(c appengine.Context) *datastore.Key {
	return datastore.NewKey(c, "VoiceMyBot", "default_voicemybot", 0, nil)
}

//Appengine initialization method - it shows
func init() {
	http.HandleFunc("/", root)
	http.HandleFunc("/hipchat_descriptor", hipchatDescriptor)
	http.HandleFunc("/hipchat_installed", hipchatInstalled)
	http.HandleFunc("/hipchat_installed/", hipchatInstalled)
	http.HandleFunc("/hipchat_admin", hipchatAdmin)
	http.HandleFunc("/hipchat_doconfigure", hipchatDoConfigure)

	http.HandleFunc("/sitePinger", sitePinger)
	http.HandleFunc("/fetchRoomHistory", fetchRoomHistory)

	http.HandleFunc("/alexa", alexaHandler)
	http.HandleFunc("/alexaLink", alexaLinkHandler)
	http.HandleFunc("/alexaLinkNotAuth", alexaLinkLanding)

	http.HandleFunc("/userLoginPage", userLoginPage)
	http.HandleFunc("/hipchat_user_configuration", hipchatUserConfiguration)
	http.HandleFunc("/hipchat_save_user_configuration", hipchatUserConfigurationSave)
	http.HandleFunc("/hipchat_glance", hipchatGlance)

}

// Checks if the site works as expected and put result to integration.
// If site is not OK - notification will be sent.
func pingSite(c appengine.Context, client *http.Client, x *Integration) {
	if x.SiteURL != "" {
		resp, err := client.Get(x.SiteURL)
		x.SiteStatus = "Site is up and running"
		if err != nil {
			x.SiteStatus = "DNS error"
			if x.SiteDownTime == 0 {
				x.sendNotification(c, "Error pinging site "+err.Error())
			}
		} else if resp.StatusCode != 200 {
			x.SiteStatus = "Site is down. Wrong return code"
			if x.SiteDownTime == 0 {
				x.sendNotification(c, "Site is down. Code "+strconv.Itoa(resp.StatusCode))
			}
		} else if x.SitePhrase != "" {
			content, readerr := ioutil.ReadAll(resp.Body)
			if readerr != nil {
				content = []byte("Unknown error")
			}
			if !strings.Contains(string(content), x.SitePhrase) {
				x.SiteStatus = "Site is down. Wrong content"
				if x.SiteDownTime == 0 {
					x.sendNotification(c, "Specified context was not found in the response during site pinging ")
				}
			}
		}
		if x.SiteStatus != "Site is up and running" {
			if x.SiteDownTime == 0 {
				x.SiteDownTime = time.Now().Unix()
			}
		} else {

			if x.SiteDownTime != 0 {
				x.sendNotification(c, "Site is up and running")
			}
			x.SiteDownTime = 0
		}
		x.SiteLastPingTime = time.Now().Unix()
	} else {
		x.SiteLastPingTime = maximumTime
	}
}

// This function will fetch room history for all integrations.
func fetchRoomHistory(w http.ResponseWriter, r *http.Request) {
	var x Integration
	c := appengine.NewContext(r)
	q := datastore.NewQuery("Integration")
	for t := q.Run(c); ; {
		x = Integration{}
		key, err := t.Next(&x)
		if err == datastore.Done {
			return
		}
		if err != nil {
			c.Errorf(err.Error())
			return
		}
		x.getRoomHistory(c, nil)
		if _, err := datastore.Put(c, key, &x); err != nil {
			c.Errorf("Error during storing integration in fetchHistory %s", err.Error())
		}
	}
}

//Checks if the site works as expected. If not
func sitePinger(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	c.Infof("Starting site pinging")
	client := createClient(c, time.Second*2)
	q := datastore.NewQuery("Integration").Filter("SiteLastPingTime <", time.Now().Add(time.Duration(-5)*time.Second).Unix())
	funcTime := time.Now().Add(time.Duration(3) * time.Minute)
	var x Integration
	for t := q.Run(c); ; {
		x = Integration{}
		key, err := t.Next(&x)
		if err == datastore.Done {
			return
		}
		if err != nil {
			c.Errorf(err.Error())
			return
		}
		if funcTime.Before(time.Now()) {
			c.Infof("Site pinger is running more than a minute, exiting")
			return
		}
		pingSite(c, client, &x)
		if _, err := datastore.Put(c, key, &x); err != nil {
			c.Errorf("Error during storing site in site pinger %s", err.Error())
		}
	}
}

//Handle / of the application. For now it performs redirection to demo.softserveinc.com/voicemybot
func root(w http.ResponseWriter, r *http.Request) {
	url := "http://demo.softserveinc.com/voicemybot/"
	w.Header().Set("Location", url)
	w.WriteHeader(http.StatusFound)

	// RFC2616 recommends that a short note "SHOULD" be included in the
	// response because older user agents may not understand 301/307.
	// Shouldn't send the response for POST or HEAD; that leaves GET.

	note := "<a href=\"" + url + "\">Found</a>.\n"
	fmt.Fprintln(w, note)
	/*c := appengine.NewContext(r)
	u := user.Current(c)
	if u == nil {
		var welcome WelcomeData
		welcome.LoginURL, _ = user.LoginURL(c, "/")
		if err := welcomePage.Execute(w, welcome); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}
	var data TemplateData
	data.LogoutURL, _ = user.LogoutURL(c, "/")

	if err := adminPage.Execute(w, data); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}*/
}
