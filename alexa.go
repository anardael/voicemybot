package voicemybot

import (
	"bytes"
	"crypto"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"appengine"
	"appengine/datastore"
	"appengine/urlfetch"
	"appengine/user"
)

//AlexaRequest is structure for alexa request
type AlexaRequest struct {
	Version string
	Session struct {
		New         bool
		SessionId   string
		Application struct {
			ApplicationId string
		}
		Attributes map[string]interface{}
		User       struct {
			UserId      string
			AccessToken string
		}
	}
	Request struct {
		Type      string
		RequestId string
		TimeStamp string
		Intent    struct {
			Name  string
			Slots map[string]struct {
				Name  string
				Value string
			}
		}
		Reason string
	}
}

type loginData struct {
	LoginURL string
}

//GeneralAlexaResponse is used as a response structure
type GeneralAlexaResponse struct {
	Version  string `json:"version"`
	Response struct {
		OutputSpeach struct {
			Type string `json:"type"`
			Text string `json:"text"`
			Ssml string `json:"ssml"`
		} `json:"outputSpeech"`
		Card struct {
			Type    string `json:"type"`
			Title   string `json:"title"`
			Content string `json:"content"`
			Text    string `json:"text"`
		} `json:"card"`
		ShouldEndSession bool `json:"shouldEndSession"`
	} `json:"response"`
}

func errorHappens() GeneralAlexaResponse {
	var resp GeneralAlexaResponse
	resp.Response.ShouldEndSession = true
	resp.Version = "1.0"
	resp.Response.OutputSpeach.Text = "Error happens during request handling."
	resp.Response.OutputSpeach.Type = "PlainText"
	resp.Response.Card.Type = "Simple"
	resp.Response.Card.Content = "Error happens during request handling."
	return resp
}

//Converts duration to the readble form
// Duration is in seconds
func formatDuration(duration int64) (result string) {
	result = ""
	if duration/60/60/24 >= 1 {
		m := ""
		if duration/60/60/24 != 1 {
			m = "s"
		}
		result += fmt.Sprintf("%d day%s,", duration/60/60/24, m)
	}
	if duration/60/60%24 >= 1 {
		m := ""
		if duration/60/60%24 != 1 {
			m = "s"
		}
		result += fmt.Sprintf("%d hour%s,", duration/60/60%24, m)
	}
	if duration/60%60 >= 1 || duration/60/60%60 >= 1 {
		m := ""
		if duration/60/60%60 != 1 {
			m = "s"
		}
		result += fmt.Sprintf("%d minute%s,", duration/60/60%60, m)
	}
	m := ""
	if duration%6 != 1 {
		m = "s"
	}
	result += fmt.Sprintf("%d second%s", duration%60, m)
	return
}

//That function serving subscripted notification reading.
//After it add all notification to response - it will clear them
//The function will also call getHistory messages to get all room messages
func handleReadNotifications(c appengine.Context, r AlexaRequest) GeneralAlexaResponse {
	var resp GeneralAlexaResponse
	resp.Response.ShouldEndSession = true
	user, key, err := getUserByHipchat(c, r.Session.User.AccessToken)
	if err != nil {
		c.Errorf("Error in handling news while getting user %s", err.Error())
		return errorHappens()
	}
	if len(user.Notifications) == 0 {
		resp.Response.OutputSpeach.Ssml = "<speak>No new notifications to read</speak>"
		resp.Response.Card.Content = "No new notifications to read"
	} else {
		resp.Response.OutputSpeach.Ssml = "<speak>"
		for _, item := range user.Notifications {
			resp.Response.OutputSpeach.Ssml += "Notification from <s>" + item.From + "</s> <s> " + item.Message + "</s> "
			resp.Response.Card.Content += "Notification from " + item.From + ": " + item.Message + "\n"
		}
		resp.Response.OutputSpeach.Ssml += "</speak>"
		user.Notifications = []SavedMessage{}
		if _, err = datastore.Put(c, key, &user); err != nil {
			c.Errorf(err.Error())
		}
	}
	resp.Version = "1.0"
	resp.Response.OutputSpeach.Type = "SSML"
	resp.Response.Card.Type = "Simple"
	resp.Response.Card.Title = "Hipchat notifications from Subscriptions"
	return resp
}

//That function will perform site pinging and produce result to alexa response
//In case the site is down - it will took data from Integration structure
func handleSiteStatus(c appengine.Context, r AlexaRequest) GeneralAlexaResponse {
	var resp GeneralAlexaResponse
	resp.Response.ShouldEndSession = true
	client := createClient(c, time.Second*2)
	siteStatus := ""
	user, _, err := getUserByHipchat(c, r.Session.User.AccessToken)
	if err != nil {
		c.Errorf("Error in handling news while getting user %s", err.Error())
		return errorHappens()
	}
	integration, _, err := user.getIntegration(c)
	resp.Response.OutputSpeach.Ssml = "<speak>No site is configured</speak>"
	resp.Response.Card.Content = "No site is configured"
	if err == nil {
		if integration.SiteURL != "" {
			pingSite(c, client, &integration)
			siteStatus = integration.SiteStatus
			if integration.SiteDownTime != 0 {
				tm := time.Now().Unix() - integration.SiteDownTime
				siteStatus += "for " + formatDuration(tm)
			}
			siteStatus += "."
			resp.Response.OutputSpeach.Ssml = "<speak>" + siteStatus + "</speak>"
			resp.Response.Card.Content = siteStatus
		}
	} else {
		c.Warningf("No integration found for user %s, error: %s", user.UserId, err.Error())
	}
	resp.Version = "1.0"
	resp.Response.OutputSpeach.Type = "SSML"
	resp.Response.Card.Type = "Simple"
	resp.Response.Card.Title = "Site status"
	return resp
}

//That function will handle send message functionality : ask hipchat to say that I ...
//It will change am to is, have to has, myself to hisself and will send notification to the room
func handleSendMessage(c appengine.Context, r AlexaRequest) GeneralAlexaResponse {
	var resp GeneralAlexaResponse
	resp.Response.ShouldEndSession = true
	user, _, err := getUserByHipchat(c, r.Session.User.AccessToken)
	if err != nil {
		c.Errorf("Error in handling news while getting user %s", err.Error())
		return errorHappens()
	}
	integration, _, err := user.getIntegration(c)
	resp.Response.OutputSpeach.Ssml = "<speak>No room is configured</speak>"
	resp.Response.Card.Content = "No room is configured"
	if err == nil {
		msg := r.Request.Intent.Slots["msg"].Value
		if len(msg) == 0 {
			resp.Response.OutputSpeach.Ssml = "<speak>VoiceMyBot. You could ask hipchat to send that you are late, or you are busy, or feel yourself bad, or have technical problems. What you want me to do? </speak>"
			resp.Response.Card.Content = "You could ask hipchat to send that you are late or you are busy or feel yourself bad or have technical problems."
			resp.Response.ShouldEndSession = false
		} else {
			msg = strings.Replace(msg, "am", "is", 1)
			msg = strings.Replace(msg, "have", "had", 1)
			msg = strings.Replace(msg, "myself", "hisself", 1)
			if user.Name == "" {
				user.Name = user.UserId
			}
			err = integration.sendNotification(c, user.Name+" wants me to say that he/she "+msg)
			if err == nil {
				resp.Response.OutputSpeach.Ssml = "<speak>Done</speak>"
				resp.Response.Card.Content = "Done"
			} else {
				resp.Response.OutputSpeach.Ssml = "<speak>There were issues sending the message</speak>"
				resp.Response.Card.Content = "There were issues sending the message"
			}
		}
	} else {
		c.Warningf("No integration found for user %s, error: %s", user.UserId, err.Error())
	}
	resp.Version = "1.0"
	resp.Response.OutputSpeach.Type = "SSML"
	resp.Response.Card.Type = "Simple"
	resp.Response.Card.Title = "message sent"
	return resp
}

//That function serving subscripted messages reading.
//After it add all messages from followers to response - it will clear them
//The function will also call getHistory messages to get all room messages
func handleReadFollowers(c appengine.Context, r AlexaRequest) GeneralAlexaResponse {
	var resp GeneralAlexaResponse
	resp.Response.ShouldEndSession = true
	user, key, err := getUserByHipchat(c, r.Session.User.AccessToken)
	if err != nil {
		c.Errorf("Error in handling news while getting user %s", err.Error())
		return errorHappens()
	}
	t, _, _ := user.getIntegration(c)
	t.getRoomHistory(c, &user)
	if len(user.FollowedMessages) == 0 {
		resp.Response.OutputSpeach.Ssml = "<speak>No new messages to read</speak>"
		resp.Response.Card.Content = "No new messages to read"
	} else {
		resp.Response.OutputSpeach.Ssml = "<speak>"
		for _, item := range user.FollowedMessages {
			resp.Response.OutputSpeach.Ssml += "Message from <s>" + item.From + "</s> <s> " + item.Message + "</s> "
			resp.Response.Card.Content += "Message from " + item.From + ": " + item.Message + "\n"
		}
		resp.Response.OutputSpeach.Ssml += "</speak>"
		user.FollowedMessages = []SavedMessage{}
		if _, err = datastore.Put(c, key, &user); err != nil {
			c.Errorf(err.Error())
		}
	}
	resp.Version = "1.0"
	resp.Response.OutputSpeach.Type = "SSML"
	resp.Response.Card.Type = "Simple"
	resp.Response.Card.Title = "Hipchat Messages from Followers"
	return resp
}

// Function will get status for the last job. It could be build or deploy, depending on alexa request
func handleLastDeploy(c appengine.Context, r AlexaRequest) GeneralAlexaResponse {
	var resp GeneralAlexaResponse
	resp.Response.ShouldEndSession = true
	user, _, err := getUserByHipchat(c, r.Session.User.AccessToken)
	if err != nil {
		c.Errorf("Error in handling news while getting user %s", err.Error())
		return errorHappens()
	}
	integration, _, err := user.getIntegration(c)
	resp.Response.OutputSpeach.Ssml = "<speak>No jenkins is configured</speak>"
	resp.Response.Card.Content = "No jenkins is configured"
	if err == nil {
		jobName := ""
		jobOK := true
		jobType := r.Request.Intent.Slots["JobName"].Value
		if r.Request.Intent.Slots["JobName"].Value != "deploy" {
			jobType = "build"
			jobName = integration.JenkinsBuildJob
			jobOK = integration.JenkinsBuildJobOK
		} else {
			jobName = integration.JenkinsDeployJob
			jobOK = integration.JenkinsDeployJobOK
		}
		if integration.JenkinsURL != "" && jobName != "" && jobOK {
			info, _ := integration.getLastBuild(c, jobName)
			jobStatus := ""
			tm := time.Now().Unix() - info.Timestamp/1000
			jobStatus += fmt.Sprintf("Last %s was %s ago %s. Status %s", jobType, formatDuration(tm), info.getTestResult(), info.Result)
			jobStatus += "."
			resp.Response.OutputSpeach.Ssml = "<speak>" + jobStatus + "</speak>"
			resp.Response.Card.Content = jobStatus
		}
	} else {
		c.Warningf("No integration found for user %s, error: %s", user.UserId, err.Error())
	}
	resp.Version = "1.0"
	resp.Response.OutputSpeach.Type = "SSML"
	resp.Response.Card.Type = "Simple"
	resp.Response.Card.Title = "Hipchat Last actions"
	return resp
}

// Function will get status for the last successful job. It could be build or deploy, depending on alexa request
func handleLastSuccessfullDeploy(c appengine.Context, r AlexaRequest) GeneralAlexaResponse {
	var resp GeneralAlexaResponse
	resp.Response.ShouldEndSession = true
	user, _, err := getUserByHipchat(c, r.Session.User.AccessToken)
	if err != nil {
		c.Errorf("Error in handling news while getting user %s", err.Error())
		return errorHappens()
	}
	integration, _, err := user.getIntegration(c)
	resp.Response.OutputSpeach.Ssml = "<speak>No jenkins is configured</speak>"
	resp.Response.Card.Content = "No jenkins is configured"
	if err == nil {
		jobName := ""
		jobOK := true
		jobType := r.Request.Intent.Slots["JobName"].Value
		if r.Request.Intent.Slots["JobName"].Value != "deploy" {
			jobType = "build"
			jobName = integration.JenkinsBuildJob
			jobOK = integration.JenkinsBuildJobOK
		} else {
			jobName = integration.JenkinsDeployJob
			jobOK = integration.JenkinsDeployJobOK
		}
		if integration.JenkinsURL != "" && jobName != "" && jobOK {
			info, _ := integration.getLastSuccessfullBuild(c, jobName)
			jobStatus := ""
			tm := time.Now().Unix() - info.Timestamp/1000
			jobStatus += fmt.Sprintf("Last successful %s was %s ago %s. ", jobType, formatDuration(tm), info.getTestResult())
			jobStatus += "."
			resp.Response.OutputSpeach.Ssml = "<speak>" + jobStatus + "</speak>"
			resp.Response.Card.Content = jobStatus
		}
	} else {
		c.Warningf("No integration found for user %s, error: %s", user.UserId, err.Error())
	}
	resp.Version = "1.0"
	resp.Response.OutputSpeach.Type = "SSML"
	resp.Response.Card.Type = "Simple"
	resp.Response.Card.Title = "Hipchat Last actions"
	return resp
}

// Function will get status for the last failed job. It could be build or deploy, depending on alexa request
func handleLastFailedDeploy(c appengine.Context, r AlexaRequest) GeneralAlexaResponse {
	var resp GeneralAlexaResponse
	resp.Response.ShouldEndSession = true
	user, _, err := getUserByHipchat(c, r.Session.User.AccessToken)
	if err != nil {
		c.Errorf("Error in handling news while getting user %s", err.Error())
		return errorHappens()
	}
	integration, _, err := user.getIntegration(c)
	resp.Response.OutputSpeach.Ssml = "<speak>No jenkins is configured</speak>"
	resp.Response.Card.Content = "No jenkins is configured"
	if err == nil {
		jobName := ""
		jobOK := true
		jobType := r.Request.Intent.Slots["JobName"].Value
		if r.Request.Intent.Slots["JobName"].Value != "deploy" {
			jobType = "build"
			jobName = integration.JenkinsBuildJob
			jobOK = integration.JenkinsBuildJobOK
		} else {
			jobName = integration.JenkinsDeployJob
			jobOK = integration.JenkinsDeployJobOK
		}
		if integration.JenkinsURL != "" && jobName != "" && jobOK {
			info, _ := integration.getLastFailedBuild(c, jobName)
			jobStatus := ""
			tm := time.Now().Unix() - info.Timestamp/1000
			jobStatus += fmt.Sprintf("Last failed %s was %s ago %s.", jobType, formatDuration(tm), info.getTestResult())
			jobStatus += "."
			resp.Response.OutputSpeach.Ssml = "<speak>" + jobStatus + "</speak>"
			resp.Response.Card.Content = jobStatus
		}
	} else {
		c.Warningf("No integration found for user %s, error: %s", user.UserId, err.Error())
	}
	resp.Version = "1.0"
	resp.Response.OutputSpeach.Type = "SSML"
	resp.Response.Card.Type = "Simple"
	resp.Response.Card.Title = "Hipchat Last actions"
	return resp
}

//That function could run jenkins build or jenkins deploy job
func handleRunJob(c appengine.Context, r AlexaRequest) GeneralAlexaResponse {
	var resp GeneralAlexaResponse
	resp.Response.ShouldEndSession = true
	user, _, err := getUserByHipchat(c, r.Session.User.AccessToken)
	if err != nil {
		c.Errorf("Error in handling news while getting user %s", err.Error())
		return errorHappens()
	}
	integration, _, err := user.getIntegration(c)
	resp.Response.OutputSpeach.Ssml = "<speak>No jenkins is configured</speak>"
	resp.Response.Card.Content = "No jenkins is configured"
	if err == nil {
		jobName := ""
		jobOK := true
		//	jobType := r.Request.Intent.Slots["JobName"].Value
		if r.Request.Intent.Slots["JobName"].Value != "deploy" {
			//	jobType = "build"
			jobName = integration.JenkinsBuildJob
			jobOK = integration.JenkinsBuildJobOK
		} else {
			jobName = integration.JenkinsDeployJob
			jobOK = integration.JenkinsDeployJobOK
		}
		if integration.JenkinsURL != "" && jobName != "" && jobOK {
			info, _ := integration.getLastSuccessfullBuild(c, jobName)
			err := integration.executeJob(c, jobName)

			jobStatus := ""
			if err == nil {
				tm := info.Duration / 1000
				jobStatus += fmt.Sprintf("Build of %s job initiated. Estimated time %s", jobName, formatDuration(int64(tm)))
			} else {
				jobStatus = "Failed to run the job"
			}
			jobStatus += "."
			resp.Response.OutputSpeach.Ssml = "<speak>" + jobStatus + "</speak>"
			resp.Response.Card.Content = jobStatus
		}
	} else {
		c.Warningf("No integration found for user %s, error: %s", user.UserId, err.Error())
	}
	resp.Version = "1.0"
	resp.Response.OutputSpeach.Type = "SSML"
	resp.Response.Card.Type = "Simple"
	resp.Response.Card.Title = "Hipchat Last actions"

	return resp
}

// That function will read the history for the room chat
// and than remove all notifications, messages from followers and overall messages
// from the user data
func handleClear(c appengine.Context, r AlexaRequest) GeneralAlexaResponse {
	var resp GeneralAlexaResponse
	resp.Response.ShouldEndSession = true
	user, key, err := getUserByHipchat(c, r.Session.User.AccessToken)
	if err != nil {
		c.Errorf("Error in handling news while getting user %s", err.Error())
		return errorHappens()
	}
	t, _, _ := user.getIntegration(c)
	t.getRoomHistory(c, &user)
	user.LastMessages = []SavedMessage{}
	user.FollowedMessages = []SavedMessage{}
	user.Notifications = []SavedMessage{}
	if _, err = datastore.Put(c, key, &user); err != nil {
		c.Errorf(err.Error())
	}
	resp.Response.OutputSpeach.Ssml = "<speak>All messages removed from VoiceMyBot</speak>"
	resp.Response.Card.Content = "All messages removed from VoiceMyBot"
	resp.Version = "1.0"
	resp.Response.OutputSpeach.Type = "SSML"
	resp.Response.Card.Type = "Simple"
	resp.Response.Card.Title = "Hipchat Last actions"
	return resp
}

//That function serving recent messages reading.
//After it add all messages  to response - it will clear them
//The function will also call getHistory messages to get all room messages
func handleReadMessages(c appengine.Context, r AlexaRequest) GeneralAlexaResponse {
	var resp GeneralAlexaResponse
	resp.Response.ShouldEndSession = true
	user, key, err := getUserByHipchat(c, r.Session.User.AccessToken)
	if err != nil {
		c.Errorf("Error in handling news while getting user %s", err.Error())
		return errorHappens()
	}
	t, _, _ := user.getIntegration(c)
	t.getRoomHistory(c, &user)
	if len(user.LastMessages) == 0 {
		resp.Response.OutputSpeach.Ssml = "<speak>No new messages to read</speak>"
		resp.Response.Card.Content = "No new messages to read"
	} else {
		resp.Response.OutputSpeach.Ssml = "<speak>"
		for _, item := range user.LastMessages {
			resp.Response.OutputSpeach.Ssml += item.Type + " from <s>" + item.From + "</s> <s> " + item.Message + "</s> "
			resp.Response.Card.Content += item.Type + " from " + item.From + ": " + item.Message + "\n"
		}
		resp.Response.OutputSpeach.Ssml += "</speak>"
		user.LastMessages = []SavedMessage{}
		if _, err = datastore.Put(c, key, &user); err != nil {
			c.Errorf(err.Error())

		}
	}
	resp.Version = "1.0"
	resp.Response.OutputSpeach.Type = "SSML"
	resp.Response.Card.Type = "Simple"
	resp.Response.Card.Title = "Hipchat Last actions"
	return resp
}

// That function will handle overall hipchat status request
// It will response with a number of messages for the user as well as
// jenkins status and site status
// The function will also call fetching room history
func handleNews(c appengine.Context, r AlexaRequest) GeneralAlexaResponse {
	siteStatus := ""
	siteStatusSsml := ""
	jenkinsStatus := ""
	user, _, err := getUserByHipchat(c, r.Session.User.AccessToken)
	if err != nil {
		c.Errorf("Error in handling news while getting user %s", err.Error())
		return errorHappens()
	}
	integration, _, err := user.getIntegration(c)
	if err == nil {
		integration.getRoomHistory(c, &user)
		/*if integration.JenkinsBuildJob != "" && integration.JenkinsBuildJobOK {
			build, err := integration.getJobInfo(c, integration.JenkinsBuildJob)
			if err == nil {
				jenkinsStatus += "Build job " + strings.Replace(build.HealthReport[0].Description, "Build stability", "", 1) + " "
			}
		}
		if integration.JenkinsDeployJob != "" && integration.JenkinsDeployJobOK {
			deploy, err := integration.getJobInfo(c, integration.JenkinsDeployJob)
			if err == nil {
				jenkinsStatus += "Deploy job " + strings.Replace(deploy.HealthReport[0].Description, "Build stability", "", 1) + " "
			}
		}*/
		if integration.SiteURL != "" && integration.SiteStatus != "" {
			siteStatus = integration.SiteStatus
			siteStatusSsml = integration.SiteStatus
			if integration.SiteDownTime != 0 {
				tm := time.Now().Unix() - integration.SiteDownTime
				siteStatus += fmt.Sprintf(" for %s", formatDuration(tm))
				siteStatusSsml += fmt.Sprintf(" for %s", formatDuration(tm))
			}
			siteStatus += "."
			siteStatusSsml += "."
		}
	} else {
		c.Warningf("No integration found for user %s, error: %s", user.UserId, err.Error())
	}
	var resp GeneralAlexaResponse
	resp.Response.ShouldEndSession = true
	resp.Version = "1.0"
	resp.Response.OutputSpeach.Ssml = fmt.Sprintf(`<speak>
		You have %d important messages <break strength="medium"/>
		%d notification <break strength="medium"/>
		and %d overall messages.
		%s %s
	</speak>`, len(user.FollowedMessages), len(user.Notifications), len(user.LastMessages), siteStatusSsml, jenkinsStatus)
	resp.Response.OutputSpeach.Type = "SSML"
	resp.Response.Card.Type = "Simple"
	resp.Response.Card.Title = "VoiceMyBot News"
	resp.Response.Card.Content = fmt.Sprintf(`You have %d messages from followers	
		%d notification 
		and %d overall messages.
		%s %s`, len(user.FollowedMessages), len(user.Notifications), len(user.LastMessages), siteStatus, jenkinsStatus)
	return resp
}

//Function performs operational routine in handling alexa request.
//It is performing application id checking, certificate checking
// Time checking, etc
func alexaHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	var alexaRequest AlexaRequest
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		c.Errorf("Error during Alexa request decoding: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	decoder := json.NewDecoder(bytes.NewReader(body))
	err = decoder.Decode(&alexaRequest)
	if err != nil {
		c.Errorf("Error during Alexa request decoding: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	
	certificateURL := r.Header.Get("Signaturecertchainurl")
	c.Infof("Got request from alexa with cert "+certificateURL)
	if !strings.Contains(certificateURL, "/echo.api/") || (!strings.HasPrefix(strings.ToLower(certificateURL), "https://s3.amazonaws.com/echo.api/") && !strings.HasPrefix(strings.ToLower(certificateURL), "https://s3.amazonaws.com:443/echo.api/")) {
		c.Errorf("Error during Alexa request certificate chain validation %s", certificateURL)
		http.Error(w, "Error during Alexa request certificate chain validation", http.StatusUnauthorized)
		return
	}
	client := urlfetch.Client(c)
	certificate, err := client.Get(certificateURL)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	defer certificate.Body.Close()
	pembytes, err := ioutil.ReadAll(certificate.Body)
	if err != nil {
		c.Errorf("Error during Alexa request decoding: %s", err.Error())
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	block, _ := pem.Decode(pembytes)
	if err != nil {
		c.Errorf("Error during Alexa certificate pem decoding: %s", err.Error())
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	certObject, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		c.Errorf("Error during Alexa certificate decoding: %s", err.Error())
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	/*roots := x509.NewCertPool()
	rootCertbytes, err := ioutil.ReadFile("cacert.pem")
	ok := roots.AppendCertsFromPEM([]byte(rootCertbytes))
    if !ok {
        c.Errorf("failed to parse root certificate")
		http.Error(w, "Root cert error", http.StatusUnauthorized)
		return
    }
	
	opts := x509.VerifyOptions{
		DNSName: "echo-api.amazon.com",
		Roots:   roots,
	}
	if _, err = certObject.Verify(opts); err != nil {
		c.Errorf("Error during Alexa certificate verification: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}*/
	c.Infof("Certificate verified ")
	encryptedSig, err := base64.StdEncoding.DecodeString(r.Header.Get("Signature"))
	if err != nil {
		c.Errorf("Error during Alexa signature decoding: %s", err.Error())
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	publicKey := certObject.PublicKey

	// Make the request body SHA1 and verify the request with the public key

	hash := sha1.New()
	_, err = io.Copy(hash, bytes.NewReader(body))
	if err != nil {
		c.Errorf("Error during Alexa sha1 hash creation: %s", err.Error())
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	err = rsa.VerifyPKCS1v15(publicKey.(*rsa.PublicKey), crypto.SHA1, hash.Sum(nil), encryptedSig)
	if err != nil {
		c.Errorf("Error during Alexa PKCS1v15 decoding: %s", err.Error())
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	reqTimestamp, _ := time.Parse("2006-01-02T15:04:05Z", alexaRequest.Request.TimeStamp)
	if time.Since(reqTimestamp) >= time.Duration(150)*time.Second {
		c.Errorf("Request is too old %s", alexaRequest.Request.TimeStamp)
		http.Error(w, "Request is too old", http.StatusBadRequest)
		return
	}
	if alexaRequest.Session.Application.ApplicationId != "amzn1.ask.skill.6bf1e3be-c838-471b-b749-9b471624ea39" {
		c.Errorf("Wrong application id %s", alexaRequest.Session.Application.ApplicationId)
		http.Error(w, "Wrong application id", http.StatusBadRequest)
		return
	}

	var resp GeneralAlexaResponse
	if alexaRequest.Session.User.AccessToken != "" {
		key, err := datastore.DecodeKey(alexaRequest.Session.User.AccessToken)
		if err != nil {
			c.Errorf("Wrong accesstoken %s", err.Error())
			alexaRequest.Session.User.AccessToken = ""
		} else {
			var user User
			err = datastore.Get(c, key, &user)
			if err != nil {
				c.Errorf("Wrong accesstoken %s", err.Error())
				alexaRequest.Session.User.AccessToken = ""
			} else {
				alexaRequest.Session.User.AccessToken = user.UserId
			}
		}
	}
	accError := false
	if alexaRequest.Session.User.AccessToken != "" {
		aus, _, err := getUserByHipchat(c, alexaRequest.Session.User.AccessToken)
		if err != nil {
			accError = true
		}
		if !accError {
			_, _, err = aus.getIntegration(c)
			if err != nil {
				accError = true
			}
		}
	}
	resp.Response.ShouldEndSession = true
	if accError {
		resp.Version = "1.0"
		resp.Response.OutputSpeach.Text = "You must have a hipchat room linked with this account."
		resp.Response.OutputSpeach.Type = "PlainText"
		resp.Response.Card.Type = "Simple"
		resp.Response.Card.Title = "Please install hipchat add-on"
		resp.Response.Card.Content = "You must have a hipchat room linked with this account.\\n Please take a look to http://demo.softserveinc.com/voicemybot/step-by-step-instructions/ \\n . You could also ask your questiion at https://answers.atlassian.com/tags/addon-com.softserveinc.voicemybot"
		resp.Response.ShouldEndSession = true
	} else if alexaRequest.Session.User.AccessToken == "" {
		resp.Version = "1.0"
		resp.Response.OutputSpeach.Text = "You must have a google account to use this skill. Please use the Alexa app to link your Amazon account with your Google Account."
		resp.Response.OutputSpeach.Type = "PlainText"
		resp.Response.Card.Type = "LinkAccount"
		resp.Response.ShouldEndSession = true
	} else {
		c.Infof("Alexa request from registered user %s", alexaRequest.Session.User.AccessToken)
		switch {
		case alexaRequest.Request.Intent.Name == "News":
			resp = handleNews(c, alexaRequest)
		case alexaRequest.Request.Intent.Name == "Followers":
			resp = handleReadFollowers(c, alexaRequest)
		case alexaRequest.Request.Intent.Name == "Notifications":
			resp = handleReadNotifications(c, alexaRequest)
		case alexaRequest.Request.Intent.Name == "SiteStatus":
			resp = handleSiteStatus(c, alexaRequest)
		case alexaRequest.Request.Intent.Name == "AllMessages":
			resp = handleReadMessages(c, alexaRequest)
		case alexaRequest.Request.Intent.Name == "Clear":
			resp = handleClear(c, alexaRequest)
		case alexaRequest.Request.Intent.Name == "LastBuildTime":
			resp = handleLastDeploy(c, alexaRequest)
		case alexaRequest.Request.Intent.Name == "LastSuccessfulBuildTime":
			resp = handleLastSuccessfullDeploy(c, alexaRequest)
		case alexaRequest.Request.Intent.Name == "LastFailedBuildTime":
			resp = handleLastFailedDeploy(c, alexaRequest)
		case alexaRequest.Request.Intent.Name == "RunJenkins":
			resp = handleRunJob(c, alexaRequest)
		case alexaRequest.Request.Intent.Name == "SendMesssage":
			resp = handleSendMessage(c, alexaRequest)
		case alexaRequest.Request.Intent.Name == "AMAZON.HelpIntent":
			resp.Version = "1.0"
			resp.Response.OutputSpeach.Ssml = "<speak>For more detailed interactions overview, you can check configuration tab in Voice My Bot Extension settings in the Hip Chat Room. After setting up your Jenkins settings, you can say \"Build Jenkins\", or \"Deploy Jenkins\", or say \"Site Status\". But before that You can get update by asking voice my bot \"What happened\", or request \"User Feed\", or \"Notification feed\", or \"notify that you are late, or busy\". What Would You Choose?</speak>"
			resp.Response.OutputSpeach.Type = "SSML"
			resp.Response.Card.Type = "Simple"
			resp.Response.Card.Title = "VoiceMyBot Commands"
			resp.Response.Card.Content = "For more detailed interactions overview, you can check configuration tab in Voice My Bot Extension settings in the HipChat Room. After setting up your Jenkins settings you can say \"Build Jenkins\" or \"Deploy Jenkins\" or say \"Site Status\". But before that You can get update by asking voice my bot \"What happened\" or request \"User Feed\" or \"Notification feed\" or \"notify that you are late, or busy\". What Would You Choose?"
			resp.Response.ShouldEndSession = false
		case alexaRequest.Request.Intent.Name == "AMAZON.StopIntent":
			resp.Version = "1.0"
			resp.Response.OutputSpeach.Ssml = "<speak>OK</speak>"
			resp.Response.OutputSpeach.Type = "SSML"
			resp.Response.Card.Type = "Simple"
			resp.Response.Card.Title = "StopCommand"
			resp.Response.Card.Content = ""
			resp.Response.ShouldEndSession = true
		case alexaRequest.Request.Intent.Name == "AMAZON.CancelIntent":
			resp.Version = "1.0"
			resp.Response.OutputSpeach.Ssml = "<speak>OK</speak>"
			resp.Response.OutputSpeach.Type = "SSML"
			resp.Response.Card.Type = "Simple"
			resp.Response.Card.Title = "StopCommand"
			resp.Response.Card.Content = ""
			resp.Response.ShouldEndSession = true

		default:
			resp.Version = "1.0"
			resp.Response.OutputSpeach.Ssml = "<speak>VoiceMyBot. You can get update by saying \"What happened\", also you can say \"User Feed\", or \"Notification feed\", or perform Jenkins Build, or Deploy task. What Would You Choose?</speak>"
			resp.Response.OutputSpeach.Type = "SSML"
			resp.Response.Card.Type = "Simple"
			resp.Response.Card.Content = "You can get update by saying \"What happened\" also you can say \"User Feed\" or \"Notification feed\" or give Jenkins Build or Deploy task. What Would You Choose?"
			resp.Response.ShouldEndSession = false

		}
	}

	binaryResp, err := json.Marshal(resp)
	if err != nil {
		c.Errorf("Error during Alexa request response writing: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(binaryResp)
}

var loginPage = template.Must(template.New("loginPage").Parse(`
<html>
<head>
<title>VoiceMyBot login </title>
<style >
@import url(https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,700);

* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

:before, :after {
  content: '';
  display: block;
  position: absolute;
  box-sizing: border-box;
}

html, body {
  height: 100%;
  background-image: url("background1.png");
    background-size: 100% 100%;
    background-repeat: no-repeat;
  background-color: rgb(26, 29, 75);
}

.section {
  padding: 20px;

  height:70%;
  display: flex;
  align-items: center;
  justify-content: center
}

.btn {
  position: relative;
  display: block;
  width: 210px;
  height: 40px;
  margin: 10px auto;
  padding: 0 0 0 55px;
  font: 700 16px/40px 'Quattrocento Sans', sans-serif;
  text-decoration: none;
  text-transform: uppercase;
  color: #555;
  border-radius: 2px;
  background: linear-gradient(to bottom, #ffffff 0%, #e6e6e6 100%);
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.5);
  cursor: pointer;
}

.btn span {
  font-size: 14px;
  text-transform: none;
  color: #aaa;
}




.btn:hover {
  text-shadow: 0 0 10px #fff;
}

/* reverse */
.btn.reverse {
  color: #fff;
  
}

.btn.reverse span {
  color: #eee;
}


.btn.gp.reverse {
  background: linear-gradient(to bottom, #dd4433 0%, #bd2f20 100%);
  
}


.btn.reverse:before {
  color: #555;
  background: linear-gradient(to bottom, #ffffff 0%, #e6e6e6 100%);
   background: url('https://developers.google.com/identity/sign-in/g-normal.png') transparent 5px 50% no-repeat;
}
.btn:before {
  top: 0;
  left: 0;
  width: 40px;
  height: 40px;
  border-radius: 2px 0 0 2px;
    background: url('https://developers.google.com/identity/sign-in/g-normal.png') transparent 5px 50% no-repeat;
      display: inline-block;
      vertical-align: middle;
      

}
/* naked */
.btn.naked:before {
  border-right: 1px solid #aaa;
  background: none;
}

.btn.gp.naked:before {
  color: #d33523;
}


.header {
    margin-left: auto;
    margin-right: auto;
	display: block;
   width:400px;
     }
	 .headimg {
	  width:50px;
	   
	  
	 }
	 .block{
	  display: inline-block;
	 }
</style>
</head>
<body>

<div class='section'>
<div>
<div class=" header">
 <div class="block"><img src="pngRobot.png" class="headimg"/></div>
 <div class="block"><h2 style="width:320px; color:white;"> Please login to VoiceMyBot using your google account </h2></div>
 </div>
 
  <a class='btn gp reverse' href="{{.LoginURL}}">
    login
    <span>
      with Google+
    </span>
  </a>
 </div>
</div>

</body>
</html>
`))

//Function handles alexa account linking
func alexaLinkHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := user.Current(c)
	_, key, err := getUserByHipchat(c, strings.ToLower(u.Email))
	r.ParseForm()
	if err != nil {
		key = createUser(c, strings.ToLower(u.Email), r.Form.Get("state"), u.String())
	}
	if u == nil {
		http.Error(w, "User should not be nil", http.StatusBadRequest)
		return
	}
	url := r.Form.Get("redirect_uri") + "#state=" + r.Form.Get("state") + "&access_token=" + key.Encode() + "&token_type=Bearer"

	w.Header().Set("Location", url)
	w.WriteHeader(http.StatusFound)

	// RFC2616 recommends that a short note "SHOULD" be included in the
	// response because older user agents may not understand 301/307.
	// Shouldn't send the response for POST or HEAD; that leaves GET.
	note := "<a href=\"" + url + "\">Found</a>.\n"
	fmt.Fprintln(w, note)
}

//Function handles alexa account linking
func alexaLinkLanding(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	URL := "alexaLink?state=" + r.Form.Get("state") + "&redirect_uri=" + url.QueryEscape(r.Form.Get("redirect_uri"))
	var login loginData
	login.LoginURL = URL
	if err := loginPage.Execute(w, login); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
