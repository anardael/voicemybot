# README #


SoftServe’s VoiceMyBot is a chatbot with voice interface created by SoftServe to access all of Atlassian HipChat with thousands of its Marketplace add-ons from a single place, by using voice commands. Unlike common personal assistant apps, you can configure the kind of information/updates you’d like to receive, and – since it’s open source – you can add integrations of your own. 

To learn more, download VoiceMyBot and get step-by-step installation and configuration instructions here: [VoiceMyBot.com ](https://voicemybot.com)

Application is design to work on google app engine so you could visit https://cloud.google.com/go/home for more details

 Please contact [Dmytro Igonkin](mailto:digonkin@softserveinc.com) for any questions.

This code is licenced under [GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html)