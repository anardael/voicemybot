package voicemybot

import (
	"testing"
)

func TestVerifyToken(t *testing.T) {
	s, _ := verifyToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0NjkwMjI4MTYsImlzcyI6IjNlZDAwNjcwLThlYzEtNDA4OS05NWQ0LWI5MDI4YzcyZTQ2NCIsInBybiI6IjM3NzY5MzYiLCJqdGkiOiJBMFFxcWU3U05yeXcxVWVtVHpWbCIsImNvbnRleHQiOnsicm9vbV9pZCI6MjYyMDk2OSwicm9vbUlkIjoyNjIwOTY5LCJ1c2VyX3R6IjoiRXVyb3BlL0hlbHNpbmtpIn0sImlhdCI6MTQ2OTAyMTkxNiwic3ViIjoiMzc3NjkzNiJ9.inGpDR8knpYIP8tbnYHMN8dbILqxYjvtC/oEgKn1fB0", "plqTi4KufPO6YQjye3hObNynnlzz8NGWTXGlwY2J")
	if !s {
		t.Error("verifyToken fails")
	}
	s, _ = verifyToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0NjkwMjI4MTYsImlzcyI6IjNlZrAwNjcwLThlYzEtNDA4OS05NWQ0LWI5MDI4YzcyZTQ2NCIsInBybiI6IjM3NzY5MzYiLCJqdGkiOiJBMFFxcWU3U05yeXcxVWVtVHpWbCIsImNvbnRleHQiOnsicm9vbV9pZCI6MjYyMDk2OSwicm9vbUlkIjoyNjIwOTY5LCJ1c2VyX3R6IjoiRXVyb3BlL0hlbHNpbmtpIn0sImlhdCI6MTQ2OTAyMTkxNiwic3ViIjoiMzc3NjkzNiJ9.inGpDR8knpYIP8tbnYHMN8dbILqxYjvtC/oEgKn1fB0", "plqTi4KufPO6YQjye3hObNynnlzz8NGWTXGlwY2J")
	if s {
		t.Error("verifyToken fails")
	}
	s, _ = verifyToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE0NjkwMjI4MTYsImlzcyI6IjNlZDAwNjcwLThlYzEtNDA4OS05NWQ0LWI5MDI4YzcyZTQ2NCIsInBybiI6IjM3NzY5MzYiLCJqdGkiOiJBMFFxcWU3U05yeXcxVWVtVHpWbCIsImNvbnRleHQiOnsicm9vbV9pZCI6MjYyMDk2OSwicm9vbUlkIjoyNjIwOTY5LCJ1c2VyX3R6IjoiRXVyb3BlL0hlbHNpbmtpIn0sImlhdCI6MTQ2OTAyMTkxNiwic3ViIjoiMzc3NjkzNiIgfQ==.inGpDR8knpYIP8tbnYHMN8dbILqxYjvtC/oEgKn1fB0", "plqTi4KufPO6YQjye3hObNynnlzz8NGWTXGlwY2J")
	if s {
		t.Error("verifyToken fails")
	}
}
func TestHtml2Text(t *testing.T) {
	s := "<img src=\"https://ssl.gstatic.com/docs/doclist/images/icon_11_spreadsheet_list.png\" /> Michael has created a spreadsheet: <a href=\"https://docs.google.com/a/atlassian.com/spreadsheets/d/1/edit?usp=drivesdk\">my money</a>."
	if html2Text(s) != "Michael has created a spreadsheet:  my money ." {
		t.Error("HTML2 text returns wrong data ," + html2Text(s))
	}
}
